<?php
/*=========================================================+
|| # Azure Kernel of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|| # Azure Kernel 3.0
|+=========================================================+
|| # XDR 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

// Inicio

define('IN_AZURE', TRUE);
if(array_key_exists('HTTP_CLIENT_IP', $_SERVER))
	define('MY_IP', str_replace('::1', '127.0.0.1', $_SERVER['HTTP_CLIENT_IP']));
elseif(array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER))
	define('MY_IP', str_replace('::1', '127.0.0.1', $_SERVER['HTTP_X_FORWARDED_FOR']));
elseif(array_key_exists('REMOTE_ADDR', $_SERVER))
	define('MY_IP', str_replace('::1', '127.0.0.1', $_SERVER['REMOTE_ADDR']));
else
	exit;


define('DS', DIRECTORY_SEPARATOR);
define('KERNEL', dirname(__FILE__) . DS);
define('LANGUAGES', KERNEL . 'Lang' . DS);
define('Files', KERNEL . 'Files' . DS);

date_default_timezone_set('Europe/Madrid');
ob_start('fatal_error_handler');
register_shutdown_function('__ENDSCRIPT');

function fatal_error_handler($buffer){
    $error = error_get_last();
    if($error['type'] == 1):
		require 'Error500.php';
		return $newBuffer;
	endif;

    return $buffer;
}

function __ENDSCRIPT(){
	if(isset($GLOBALS['MySQLi']))
		$GLOBALS['MySQLi']->close();
	if(class_exists('SOCKETS'))
		SOCKETS::CLOSE();
}

ini_set('default_charset', 'utf-8');
ini_set('expose_php', 0);
ini_set('session.name', 'aXDR-RTM:1');
ini_set('session.gc_probability', 10);
ini_set('session.gc_divisor', 100);
ini_set('session.cookie_httponly', 1);
ini_set('session.gc_maxlifetime', 600);
ini_set('zlib_output_compression', 'On');

//error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_WARNING);
$_SERVER['HTTP_USER_AGENT'] = isset($_SERVER['HTTP_USER_AGENT']) ? str_replace(['<', '>', '"', '\'', '\\'], ['&lt;', '&gt;', '&quot;', '&#39;', '&#92;'], $_SERVER['HTTP_USER_AGENT']) : '';
$_SERVER['REQUEST_URI'] = str_replace('.php', '', strtolower($_SERVER['REQUEST_URI']));

if(strstr($_SERVER['REQUEST_URI'], '?'))
	define ('URI', explode('?', $_SERVER['REQUEST_URI'])[0]);
else
	define ('URI', $_SERVER['REQUEST_URI']);

@session_start();
// AntiHTML & SQL Injection
if(count($_POST) > 50 || count($_GET) > 50)
	exit;

$rKeys = get_html_translation_table(HTML_ENTITIES, ENT_HTML401 | ENT_QUOTES, 'ISO-8859-1');
$rKeys['\\'] = '&#92;';

foreach($_POST as $Key => $Value)
	$_POST[$Key] = str_replace(array_keys($rKeys), array_values($rKeys), mb_convert_encoding($Value, 'ISO-8859-1', mb_detect_encoding($Value, 'UTF-8,ISO-8859-1,ISO-8859-15', true)));
foreach($_GET as $Key => $Value)
	$_GET[$Key] = str_replace(array_keys($rKeys), array_values($rKeys), mb_convert_encoding($Value, 'ISO-8859-1', mb_detect_encoding($Value, 'UTF-8,ISO-8859-1,ISO-8859-15', true)));

$siteBlocked = false;

require 'Azure.Cache.php';
require 'Azure.Methods.php';
if(isset($LOAD)):
	foreach($LOAD as $f){
		require_once KERNEL . 'Other' . DS . $f . '.php';
	}
endif;

require 'Azure.Core.php';

if($Restrictions['Country']['Action'] > 0 && MY_IP != '127.0.0.1'):
	if(isset($_SESSION['Country'])):
		$Code = $_SESSION['Country'];
	else:
		$Code = file_get_contents('http://api.hostip.info/country.php?ip=' . $_SERVER['REMOTE_ADDR']);
		$_SESSION['Country'] = $Code;
	endif;

	if($Restrictions['Country']['Strict'] && $Code == 'XX'):
		$siteBlocked = true;
	elseif($Restrictions['Country']['Action'] == '1' && isset($Restrictions['Country']['List'][$Code])):
		$siteBlocked = true;
	elseif($Restrictions['Country']['Action'] == '2' && !isset($Restrictions['Country']['List'][$Code])):
		$siteBlocked = true;
	endif;
endif;

require 'Azure.User.php';
	USER::CHECK();

if(($Restrictions['Maintenance']['Active'] && ((USER::$LOGGED && USER::$Data['Rank'] < $Restrictions['Maintenance']['Except']) || !USER::$LOGGED)) && !isset($_uMaintenance)):
	require Files . $Config['Lang'] . DS . 'HTMLs' . DS . 'Maintenance.html';
	exit;
endif;

//LOL
header('X-Origin-Id: resin-fe-4');

###############################LoadFILES###################################
DEFINE('SCRIPT', Files . $Config['Lang'] . DS . 'Scripts' . DS);
DEFINE('HEADER', Files . $Config['Lang'] . DS . 'Templates' . DS . 'Headers' . DS);
DEFINE('FOOTER', Files . $Config['Lang'] . DS . 'Templates' . DS . 'Footers' . DS);
DEFINE('HTML', Files . $Config['Lang'] . DS . 'HTMLs' . DS);
DEFINE('ACP', Files . $Config['Lang'] . DS . 'ACP' . DS);
?>