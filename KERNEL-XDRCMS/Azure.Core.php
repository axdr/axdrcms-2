<?php
/*=========================================================+
|| # Azure Kernel of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

require(KERNEL .  '/User.Config.php');
if(empty($_SERVER['SERVER_NAME']))
	exit;

$Config['URL']['Default']['Server'] = strtolower($Config['URL']['Default']['Server']);
$Config['URL']['devPrivateServer'] = strtolower($Config['URL']['devPrivateServer']);
$_Server = str_replace('www.', '', $_SERVER['SERVER_NAME']);

if(!empty($Config['URL']['devPrivateServer']) && ($Config['URL']['devPrivateServer'] === $_SERVER['SERVER_NAME'])):
	$Config['Lang'] = $Config['URL']['Default']['Lang'];
	$_uMaintenance = true;
	define('PATH', 'http://' . $_SERVER['SERVER_NAME']);
	define('www', $_SERVER['SERVER_NAME']);
	define('SPATH', 'http://' . $_SERVER['SERVER_NAME']);
else:
	$sUrl = isset($Config['URL'][$_Server]) ? $_Server : (!empty($Config['URL']['Default']['Server']) ? 'Default' : exit('No tienes acceso a esta web.'));

	define('PATH', ($Config['URL'][$sUrl]['Require.www']) ? 'http://www.' . $Config['URL'][$sUrl]['Server'] : 'http://' . $Config['URL'][$sUrl]['Server']);
	define('www', ($Config['URL'][$sUrl]['Require.www']) ? 'www.' . $_Server : $_Server);
	define('SPATH', ($Config['URL'][$sUrl]['SSL.enabled']) ? 'https://' . www : 'http://' . www);
	if($Config['URL'][$sUrl]['Require.www'] && strpos($_SERVER['SERVER_NAME'], 'www.') === FALSE):
		header('Location: ' . PATH . $_SERVER['REQUEST_URI']); exit;
	endif;
	
	$Config['Lang'] = $Config['URL'][$sUrl]['Lang'];
	if($sUrl !== 'Default')
		$Config['MySQL'] = $Config['URL'][$sUrl]['MySQL'];
endif;

#CONTROLER MODE
require 'Other/Azure.MVC.php';

$MySQLi = new mysqlic($Config['MySQL']['host'], $Config['MySQL']['user'], $Config['MySQL']['pass'], $Config['MySQL']['dbname']);

#END CONTROLER

if($MySQLi->connect_error):
	if($MySQLi->connect_errno == 1045):
		die('Error MySQLi (1045): Datos Incorrectos.');
	else:
		die('Error MySQLi (' . @$MySQLi->connect_errno . '): ' . $MySQLi->connect_error);
	endif;
endif;

$MySQLi->set_charset((strtolower(ini_get('default_charset')) === 'utf-8') ? 'utf8' : 'latin2');
$_XDRBuild = '63-BUILD2769 - 08.09.2014 12:32 - ' . $Config['Lang'] . ' - aXDR 2.0';
$mCharacter = $MySQLi->character_set_name();

#VARIABLES
$siteName = $hotelName . ' Hotel';
$charsetm = 'utf-8';
$SiteSettings = CACHE::GetAIOConfig('Site');
$defaultGallery = (($SiteSettings['defaultStyle'] == -1) ? (SPATH . '/web-gallery/63_1dc60c6d6ea6e089c6893ab4e0541ee0/2769') : ($SiteSettings['Styles'][$SiteSettings['defaultStyle']]['PATH']));

$MinRank = key($staffRanks);
end($staffRanks);
$MaxRank = key($staffRanks);
reset($staffRanks);

define('HPATH', SPATH . $Config['URL']['dirACP']);
define('LOOK', 'https://www.habbo.com/habbo-imaging/avatarimage?figure=');
define('LOOKHEAD', PATH . '/habbo-imaging/head?figure=');

if(isset($_COOKIE['customStyle']) && is_numeric($_COOKIE['customStyle'])):
	if(!isset($SiteSettings['Styles'][$_COOKIE['customStyle']]) || !$SiteSettings['Styles'][$_COOKIE['customStyle']]['Actived']):
		unset($_COOKIE['customStyle']);
		define ('webgallery', $defaultGallery);
	else:
		define ('webgallery', $SiteSettings['Styles'][$_COOKIE['customStyle']]['PATH']);
	endif;
else:
	define ('webgallery', $defaultGallery);
endif;
?>