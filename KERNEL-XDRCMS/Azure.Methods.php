<?php
class METHOD {
	private static $__SMTPDebug = 0;
	private static $_pastText = ['Hace ', 'unos ', ' segundos', ' minuto', ' hora', ' días', 'Ayer', ' semana', ' mes', ' año'];
	private static $_futureText = ['Dentro de ', 'unos ', ' segundos', ' minuto', ' hora', ' días', 'Mañana', ' semana', ' mes', ' año'];
	
	public static function RANDOM($l, $n = true, $L = true, $o = '') {
		$c = '';
		$c .= ($n) ? '0123456789' : '';
		$c .= ($L) ? 'QWERTYUIOPASDFGHJKLLZXCVBNMqwertyuiopasdfghjklzxcvbnm' : '';
		$c .= $o;

		$s = '';
		$C = 0;
		while ($C < $l){ 
			$s .= substr($c, rand(0, strlen($c) -1), 1);
			$C++;
		}

		return $s;
	}
	
	public static function HASH($s) {
		return str_replace('$2x$07$axdrcmspoweredbyxdr', '', crypt($s, '$2x$07$axdrcmspoweredbyxdrhash$'));
	}
	
	public static function TAGS($s){
		return str_replace(['%HOST%', '%NAME%'], [www, USER::$Data['Name']], $s);
	}
	
	public static function ChangeSQLCharset($s) {
		// need rework
		//if($GLOBALS['mCharacter'] === 'utf8'): 
		//	return (mb_detect_encoding($s, 'auto') === 'ASCII') ? utf8_encode($s) : $s;
		//else:
		//	return (mb_detect_encoding($s, 'auto') === 'ASCII') ? $s : utf8_decode($s);
		//endif;
		return $s;
	}
	
	public static function GetCharset($s) {
		return str_ireplace('ascii', 'ISO-8859-1', mb_detect_encoding($s));
	}

	public static function ChangeCMSCharset($s) {
		// substitution?
		return htmlspecialchars_decode(htmlentities($s, ENT_NOQUOTES, ini_get('default_charset')), ENT_NOQUOTES);
	}
	
	public static function DecodeBBText($s)
	{
		$str = preg_replace_callback('/\[code\](.*?)\[\/code\]/is', function($find) { return "[decodeNow]" . base64_encode($find[0]) . "[/decodeNow]"; } , $s);
           
		$ss = ['/\[b\](.*?)\[\/b\]/is', '/\[i\](.*?)\[\/i\]/is', '/\[u\](.*?)\[\/u\]/is', '/\[s\](.*?)\[\/s\]/is', '/\[quote\](.*?)\[\/quote\]/is', '/\[link\=(.*?)\](.*?)\[\/link\]/is', '/\[url\=(.*?)\](.*?)\[\/url\]/is', '/\[color\=(.*?)\](.*?)\[\/color\]/is', '/\[size=small\](.*?)\[\/size\]/is', '/\[size=large\](.*?)\[\/size\]/is', '/\[habbo\=(.*?)\](.*?)\[\/habbo\]/is', '/\[room\=(.*?)\](.*?)\[\/room\]/is', '/\[group\=(.*?)\](.*?)\[\/group\]/is'];
		$sr = ["<b>$1</b>", "<i>$1</i>", "<u>$1</u>", "<s>$1</s>", "<div class=\"bbcode-quote\">$1</div>", "<a href=\"$1\">$2</a>", "<a href=\"$1\">$2</a>", "<span style=\"color: $1;\">$2</span>", "<span style=\"font-size: 9px;\">$1</span>", "<span style=\"font-size: 14px;\">$1</span>", "<a href=\"/home/$1/id\">$2</a>", "<a onclick=\"roomForward(this, '$1', 'private'); return false;\" target=\"client\" href=\"/client?forwardId=2&roomId=$1\">$2</a>", "<a href=\"/groups/$1/id\">$2</a>"];
 
		$s = preg_replace ($ss, $sr, $s);
		$s = preg_replace_callback('/\[decodeNow\](.*?)\[\/decodeNow\]/is', function($find) { $p = $find[0]; $p = str_replace("[decodeNow]", "", $p);$p = str_replace("[/decodeNow]", "", $p);$p = "<pre>" . base64_decode($p) . "</pre>"; $p = str_replace("[code]", "", $p); $p = str_replace("[/code]", "", $p); return $p;}, $s);

		return $s;
	}
	
	public static function GetOnlineCount() {
		if($GLOBALS['Config']['OnlineType'] === 0):
			return $GLOBALS['MySQLi']->query('SELECT COUNT(*) FROM users WHERE online = \'1\'')->fetch_assoc()['COUNT(*)'];
		elseif($GLOBALS['Config']['OnlineType'] === 1):
			$q = $GLOBALS['MySQLi']->query('SELECT users_online FROM server_status');
			return ($q && $q->num_rows === 1) ? $q->fetch_assoc()['users_online'] : 0;
		elseif($GLOBALS['Config']['OnlineType'] === 2):
			return $GLOBALS['MySQLi']->query('SELECT COUNT(*) FROM user_online')->fetch_assoc()['COUNT(*)'];
		elseif($GLOBALS['Config']['OnlineType'] === 3):
			require_once KERNEL . 'Other' . DS . 'Azure.Sockets.php';
			return SOCKET::SEND('getonline');
		endif;
	}
	
	public static function ParseUNIXTIME($t) {
		$s = '--';
		$a = self::$_pastText;

		if(!is_numeric($t))
			return $s;

		$t = time() - $t;
		if($t == 0):
			return 'Ahora mismo.';
		elseif($t < 0):
			$t *= -1;
			$a = self::$_futureText;
		endif;

		$s = $a[0];
		if($t < 60):
			$s .= ($t < 15) ? $a[1] . $a[2] : $t . $a[2];
		elseif($t <= 3600):
			$t = round($t / 60);
			$s .= $t . $a[3] . (($t > 1) ? 's' : '');
		elseif($t < 86400):
			$t = round($t / 3600);
			$s .= $t . $a[4] . (($t > 1) ? 's' : '');
		elseif($t <= 2629800):
			$t = explode('.', ($t / 86400))[0];
			$s = ($t > 27) ? '4' . $a[7] . 's' : (($t > 20) ? '3' . $a[7] . 's' : (($t > 13) ? '2' . $a[7] . 's' : (($t > 6) ? '1' . $a[7] : (($t > 1) ? $t . $a[5] : $a[6]))));
			$s = ($s !== $a[6]) ? $a[0] . $s : $s;
		elseif($t <= 31557600):
			$t = explode('.', ($t / 2629800))[0];
			$s .= $t . $a[8] . (($t > 1) ? 's' : '');
		else:
			$t = explode('.', ($t / 31557600))[0];
			$s .= $t . $a[9] . (($t > 1) ? 's' : '');
		endif;
		
		return $s;
	}
	
	public static function CheckSTR($s, $S = true) {
		return ($S) ? preg_match('/^[ -~\\p{L}]+\\z/', $s) : preg_match('/^[-~\\p{L}]+\\z/', $s);
	}
	
	public static function SendMail($t, $T, $b) { // to, Title, body
		if(!$GLOBALS['Config']['EnabledMails'])
			return;
			
		require_once KERNEL . 'Other' . DIRECTORY_SEPARATOR . 'SMTP.Mailer.php';
		$m = new PHPMailer();
		$m->IsSMTP();
		
		$m->SMTPAuth = $GLOBALS['Config']['SMTP'][3];
		$m->SMTPSecure = $GLOBALS['Config']['SMTP'][1];
		$m->SMTPDebug = self::$__SMTPDebug;
		
		$m->Host = $GLOBALS['Config']['SMTP'][2];
		$m->Port = $GLOBALS['Config']['SMTP'][0];
		$m->Username = $GLOBALS['Config']['SMTP'][4];
		$m->Password = $GLOBALS['Config']['SMTP'][5];
		
		$m->SetFrom($GLOBALS['Config']['SMTP'][4], $GLOBALS['sitename']);
		$m->AddAddress($t);
		
		$m->IsHTML(true);
		$m->Subject = $T;
		$m->Body = $b;
		$m->MsgHTML($b);
		$m->Helo = PATH;
		$m->Timeout = 300;
		
		return $m->Send();
	}
	
	public static function CheckCaptcha($r) {
		require_once KERNEL . 'Other' . DS . 'Google.Recaptcha.php';
		return ReCaptcha::Verify($r)->IsSuccess();
	}
	
	public static function MVC() {
		self::$__SMTPDebug = 2;
	}
	
	public static function get_http_response_code($url) {
		if($url == '') return '404';
		$headers = get_headers($url);
		return substr($headers[0], 9, 3);
	}
	
}
?>