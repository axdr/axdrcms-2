<?php
$PageName = 'Styles';

function NewStyle($t, $c, $p, $v) {
	global $SiteSettings;
	$SiteSettings['StylesI'] = (isset($SiteSettings['StylesI'])) ? ++$SiteSettings['StylesI'] : 0;
	$SiteSettings['Styles'][$SiteSettings['StylesI']] = ['Title' => $t, 'Creator' => $c, 'PATH' => $p, 'Version' => $v, 'Actived' => true];
	
	CACHE::SetAIOConfig('Site', json_encode($SiteSettings));
}

if($do === 'remove' && isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] != -1):
	if(!checkAntiCsrf()): echo 'Invalid Secret Key'; exit;
	elseif(!isset($SiteSettings['Styles'][$_POST['id']])): echo 'Error 1'; exit; endif;
	
	unset($SiteSettings['Styles'][$_POST['id']]);

	CACHE::SetAIOConfig('Site', json_encode($SiteSettings));
	echo 'It has removed successfully. Reload the page.';
	exit;
elseif($do === 'acdi' && isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] != -1):
	if(!checkAntiCsrf()): echo 'Invalid Secret Key'; exit;
	elseif(!isset($SiteSettings['Styles'][$_POST['id']])): echo 'Error 1'; exit; endif;

	$SiteSettings['Styles'][$_POST['id']]['Actived'] = ($SiteSettings['Styles'][$_POST['id']]['Actived'] === false);
	if($SiteSettings['defaultStyle'] === $_POST['id']):	$SiteSettings['defaultStyle'] = -1;	endif;

	CACHE::SetAIOConfig('Site', json_encode($SiteSettings));
	echo 'It has predetermined successfully. Reload the page.';
	exit;
elseif($do === 'predetermine' && isset($_POST['id']) && is_numeric($_POST['id'])):
	if(!checkAntiCsrf()): echo 'Invalid Secret Key'; exit;
	elseif($_POST['id'] != -1 && !isset($SiteSettings['Styles'][$_POST['id']])): echo 'Error 1'; exit; endif;
	$SiteSettings['defaultStyle'] = $_POST['id'];
	CACHE::SetAIOConfig('Site', json_encode($SiteSettings));
	echo 'It has predetermined successfully. Reload the page.';
	exit;
elseif($do === 'import' && isset($_POST['code'])):
	if(!checkAntiCsrf()): echo 'Invalid Secret Key'; exit; endif;
	$code = base64_decode(hex2bin($_POST['code']));
	if($code === FALSE): echo 'Error 1'; exit; endif;

	$code = json_decode($code, true);
	if($code === NULL): echo 'Error 2'; exit;
	elseif(count($code) > 8): echo 'Error 3'; exit; endif;

	if(!isset($code['Title'], $code['Creator'], $code['PATH'], $code['Version'])): echo 'Error 4'; exit; endif;
	$code = str_replace(['<', '>', '"', '\'', '\\'], ['&lt;', '&gt;', '&quot;', '&#39;', '&#92;'], $code);
	
	NewStyle($code['Title'], $code['Creator'], $code['PATH'], $code['Version']);

	echo 'Imported sucefully. Refresh the page.';
	exit;
elseif(($do === 'new' || ($do === 'edit' && is_numeric($key) && $key != 0)) && isset($_POST['style_title'], $_POST['style_creator'], $_POST['style_path'], $_POST['style_version'])):
	if(empty($_POST['style_title']) || empty($_POST['style_creator']) || empty($_POST['style_path']) || empty($_POST['style_version'])):
		$msg_error = 'Do not leave empty fields.';
	elseif(!checkAntiCsrf()):
		$msg_error = 'Inv�lid secret key.';
	else:
		if($do === 'edit'):
			if(!isset($SiteSettings['Styles'][$key])):
				$msg_error = 'Error.';
			else:
				$SiteSettings['Styles'][$key] = ['Title' => $_POST['style_title'], 'Creator' => $_POST['style_creator'], 'PATH' => $_POST['style_path'], 'Version' => $_POST['style_version'], 'Actived' => true];
				CACHE::SetAIOConfig('Site', json_encode($SiteSettings));
			endif;
		else:
			NewStyle($_POST['style_title'], $_POST['style_creator'], $_POST['style_path'], $_POST['style_version']);
		endif;
	endif;
elseif($do === 'edit' && is_numeric($key) && $key != 0):
	$styleRow = $SiteSettings['Styles'][$key];
endif;

require HTML . 'ACP_header.html';
?>
<a onclick="Import()" href="javascript:void(0)">Importar</a>
				<table class="striped">
                    <thead>
                        <tr>
                            <th>Id</th>
							<th class="right">Web</th>
                            <th class="right">Título</th>
                            <th class="right">Creador</th>
                            <th class="right">Versi�n</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
							<td>-1.</td>
							<td class="right">www.habbo.com | <?php echo www; ?></td>
							<td class="right">Habbo Style</td>
							<td class="right">Sulake</td>
							<td class="right">63-BUILD2769 - 08.09.2014 12:32</td>
							<td class="right"><?php if($SiteSettings['defaultStyle'] != -1): ?><a href="#" onclick="Predetermine(-1)">Predeterminar</a><?php endif; ?></td>
						</tr>
<?php
if(isset($SiteSettings['Styles']) && count($SiteSettings['Styles']) > 0):
	foreach($SiteSettings['Styles'] as $Key => $Row): ?>
                        <tr>
							<td><?php echo $Key; ?>.</td>
							<td class="right"><?php echo parse_url($Row['PATH'], PHP_URL_HOST); ?></td>
							<td class="right"><?php echo $Row['Title']; ?></td>
							<td class="right"><?php echo $Row['Creator']; ?></td>
							<td class="right"><?php echo $Row['Version']; ?></td>
							<td class="right"><a href="#" onclick="Export('<?php echo $Row['Title']; ?>', '<?php echo $Row['Creator']; ?>', '<?php echo $Row['PATH']; ?>', '<?php echo $Row['Version']; ?>')">Exportar</a> <?php if($SiteSettings['defaultStyle'] != $Key): ?><a href="#" onclick="Predetermine(<?php echo $Key; ?>)">Predeterminar</a> <?php endif; ?><a href="<?php echo HPATH; ?>/index.php?p=styles&do=edit&key=<?php echo $Key; ?>">Editar</a> <a onclick="AcDi(<?php echo $Key; ?>, 'acdi')" href="#"><?php echo ($Row['Actived']) ? 'Desactivar' : 'Activar'; ?></a> <a onclick="AcDi(<?php echo $Key; ?>, 'remove')" href="#">Eliminar</a></td>
						</tr>
<?php endforeach; endif; ?>
                    </tbody>

                    <tfoot></tfoot>
                </table>
				<ul class="accordion dark" id="accordionid" data-role="accordion">
                    <li class="accordionli <?php echo (isset($PromoData)) ? 'active' : ''; ?>">
                        <a onclick="ChangeAccordion(this)" href="javascript:void(0)"><span></span>A�adir style</a>
                        <div class="accordionccn">
							<form action='<?php echo HPATH; ?>/manage?p=styles&do=new' method='post' name='theAdminForm' id='theAdminForm' class="block-content form">
							<?php echo getAntiCsrf(); ?>
								<table width='100%' cellpadding='5' align='left' border='0'>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>Título</b>
											<div class='graytext'>
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<input type='text' name='style.title' id='title' value="<?php echo (isset($PromoData)) ? $PromoData['title'] : ''; ?>" size='30' class='textinput'>
										</td>
									</tr>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>Creador</b>
											<div class='graytext'>
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<input type='text' name='style.creator' id='title' value="<?php echo (isset($PromoData)) ? $PromoData['title'] : ''; ?>" size='30' class='textinput'>
										</td>
									</tr>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>PATH</b>
											<div class='graytext'>
												Ejemplo: <?php echo PATH; ?>/styles/web-gallery2 <br />
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<textarea name='style.path' cols='150' rows='3' wrap='soft' id='sub_desc' class='multitext'><?php echo (isset($PromoData)) ? $PromoData["desc"] : ""; ?></textarea>
										</td>
									</tr>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>Versi�n</b>
											<div class='graytext'>
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<input type='text' name='style.version' id='imageurl' value="<?php echo (isset($PromoData)) ? $PromoData['img'] : ''; ?>" size='100' class='textinput' onchange="imageChanged()">
										</td>
									</tr>
									<tr>
										<td align='right' class='tablesubheader' colspan='2' >
											<input class='realbutton' type="submit" value="Add" accesskey='s'/>
										</td>
									</tr>
								</table>
							</form>
                        </div>
                    </li>

<?php if(isset($styleRow)): ?>
                    <li class="accordionli active">
                        <a onclick="ChangeAccordion(this)" href="javascript:void(0)"><span></span>Editar Style ID #<?php echo $key; ?></a>
                        <div class="accordionccn">
							<form action='<?php echo HPATH; ?>/manage?p=styles&do=edit&key=<?php echo $key; ?>' method='post' name='theAdminForm' id='theAdminForm' class="block-content form">
							<?php echo getAntiCsrf(); ?>
								<table width='100%' cellpadding='5' align='left' border='0'>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>T�tulo</b>
											<div class='graytext'>
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<input type='text' name='style.title' id='title' value="<?php echo $styleRow['Title']; ?>" size='30' class='textinput'>
										</td>
									</tr>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>Creador</b>
											<div class='graytext'>
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<input type='text' name='style.creator' id='title' value="<?php echo $styleRow['Creator']; ?>" size='30' class='textinput'>
										</td>
									</tr>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>PATH</b>
											<div class='graytext'>
												Ejemplo: /styles/web-gallery2
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<textarea name='style.path' cols='150' rows='3' wrap='soft' id='sub_desc' class='multitext'><?php echo $styleRow['PATH']; ?></textarea>
										</td>
									</tr>
									<tr>
										<td class='tablerow1' width='20%'  valign='middle'><b>Versi�n</b>
											<div class='graytext'>
											</div>
										</td>
										<td class='tablerow2' width='40%'  valign='middle'>
											<input type='text' name='style.version' id='imageurl' value="<?php echo $styleRow['Version']; ?>" size='100' class='textinput' onchange="imageChanged()">
										</td>
									</tr>
									<tr>
										<td align='right' class='tablesubheader' colspan='2' >
											<input class='realbutton' type="submit" value="Add" accesskey='s'/>
										</td>
									</tr>
								</table>
							</form>
                        </div>
                    </li>
<?php endif; ?>
                </ul>
	</div>
<script type="text/javascript">
function Predetermine(ID){
	NewDialog("Advice", get("<?php echo HPATH; ?>/manage?p=styles&do=predetermine", "POST", "id=" + ID), "", "<button onclick='CloseDialog()'>Listo</button>");
}

function AcDi(ID, o){
	NewDialog("Advice", get("<?php echo HPATH; ?>/manage?p=styles&do=" + o, "POST", "id=" + ID), "", "<button onclick='CloseDialog()'>Listo</button>");
}

function Import(){
	NewDialog("Code Import", "Inserta el c�digo que recibiste.", "<textarea id='codeJSON' style='width: 620px; height: 90px;'></textarea>", "<button class='marked' onclick='Insert()'>Listo</button><button onclick='CloseDialog()'>Cerrar</button>");
}

function Export(title, creator, path, version) {
	var Txts = {"Title": title, "Creator": creator, "PATH": path, "Version": version};
	NewDialog("Exportar", "Guardalo o compartelo con los dem�s.", "<textarea style='width: 620px; height: 90px;'>" + window.btoa(JSON.stringify(Txts)) + "</textarea>", "<button onclick='CloseDialog()'>Listo</button>");
}

function Insert(){
	var code = b2h(element("#codeJSON").value);
	CloseDialog();
	NewDialog("Aviso", get("<?php echo HPATH; ?>/manage?p=styles&do=import", "POST", "code=" + code), "", "<button onclick='CloseDialog()'>Listo</button>");
}
</script>