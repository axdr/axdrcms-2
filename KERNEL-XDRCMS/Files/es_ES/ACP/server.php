<?php
$PageName = 'Conexiones';

if($do === 'save' && isset($_POST['game_host'], $_POST['game_port'], $_POST['mus_enabled'],  $_POST['mus_host'], $_POST['mus_port'])):
	if(!checkAntiCsrf()):
		$msg_error = 'Invalid Secret Key. Please log in with a valid Secret Key';
		goto S;
	elseif(!is_numeric($_POST['game_port']) || !is_numeric($_POST['mus_port'])):
		$msg_error = 'Los puertos tienen que ser números.';
		goto S;
	endif;

	$DataToSave = ['TCP' => ['Server' => $_POST['game_host'],'Port' => $_POST['game_port']],'MUS' => ['Enabled' => ($_POST['mus_enabled'] === '1'),'Server' => $_POST['mus_host'],'Port' => $_POST['mus_port']]];
	CACHE::SetAIOConfig('Server', json_encode($DataToSave));
	SLOG('Change', 'Editado la configuración del servidor', 'manage.php[server]', 0);

	$msg_correct = 'Se han guardado los cambios con éxito.';
endif;

S:
$sSettings = (isset($DataToSave)) ? $DataToSave : CACHE::GetAIOConfig('Server');
require HTML . 'ACP_header.html';
?>
<script type="text/javascript">
function resetConfiguration()
{
	element('#game_host').value = '127.0.0.1';
	element('#game_port').value = '30001';
	element('#mus_host').value = '127.0.0.1';
	element('#mus_port').value = '30002';
}

ChangeMenuButtons("Guardar", function() { element('#form1').submit() }, "Predeterminar", function() { resetConfiguration() });
</script>
<?php if(!extension_loaded('sockets')): ?>
		<b>Aviso: No tienes la extensión de sockets activada.</b>
<?php endif; ?>
<form action='<?php echo HPATH; ?>/manage?p=server&do=save' method='post' name='theAdminForm' id='form1' class="block-content form">
<?php echo getAntiCsrf(); ?>
<table width='100%' cellspacing='9' cellpadding='5' align='center' border='0'>
<tr>
<td class='tablerow1'  width='40%'  valign='middle'><b>Servidor</b><p class='graytext'>Host o Ip.</p></td>
<td class='tablerow2'  width='60%'  valign='middle'><div class="input-control text"><input type="text" name='game_host' id='game_host' value="<?php echo $sSettings['TCP']['Server']; ?>" size='30' /></div></td>
</tr>
<td class='tablerow1'  width='40%'  valign='middle'><b>Puerto</b><div class='graytext'>Puerto del servidor.</div></td>
<td class='tablerow2'  width='60%'  valign='middle'><div class="input-control"><input type='text' name='game_port' id='game_port' value="<?php echo $sSettings['TCP']['Port']; ?>" size='30' class='textinput'></div></td>
</tr>
<tr>
<td class='tablerow1'  width='40%'  valign='middle'><b>Conexión MUS</b><div class='graytext'></div></td>
<td class='tablerow2'  width='60%'  valign='middle'>
<select name='mus_enabled' class="styled">
    <option value="1" <?php echo (($sSettings['MUS']['Enabled']) ? 'selected=true' : ''); ?>>Activado</option>
    <option value="0" <?php echo (($sSettings['MUS']['Enabled']) ? '' : 'selected=true'); ?>>Desactivado</option>
</select>
</td>
</tr>
<tr>
<td class='tablerow1'  width='40%'  valign='middle'><b>Servidor MUS</b><div class='graytext'>Host o Ip.</div></td>
<td class='tablerow2'  width='60%'  valign='middle'><div class="input-control"><input type='text' name='mus_host' id='mus_host' value="<?php echo $sSettings['MUS']['Server']; ?>" size='30' class='textinput'></div></td>
</tr>
<tr>
<td class='tablerow1'  width='40%'  valign='middle'><b>Puerto MUS</b><div class='graytext'>Puerto del servidor.</div></td>
<td class='tablerow2'  width='60%'  valign='middle'><div class="input-control"><input type='text' name='mus_port' id='mus_port' value="<?php echo $sSettings['MUS']['Port']; ?>" size='30' class='textinput'></div></td>
</tr>
</table>
</div>
</div>
</form>