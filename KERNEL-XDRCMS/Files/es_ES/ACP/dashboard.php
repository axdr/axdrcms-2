<?php
if($do === 'new' && isset($_POST['title'], $_POST['body'])):
	if(!checkAntiCsrf()):
		$msg_error = 'Invalid Secret Key.';
	elseif(strlen($_POST['title']) < 4):
		$msg_error = 'Título muy corto.';
	else:
		$MySQLi->query('INSERT INTO xdrcms_acp_comments (userID, Title, Body, Created) VALUES (' . USER::$Data['ID'] . ', \'' . $_POST['title'] . '\', \'' . $_POST['body'] . '\', ' . time() . ')');
		$MySQLi->query('INSERT INTO xdrcms_staff_log (action,message,note,userid,targetid,timestamp) VALUES (\'ACP\',\'Writed new commentary.\',\'dashboard.php\', ' . USER::$Data['ID'] . ', 0,' . time() .')');
	endif;
endif;

$PageName = 'Principal';
require HTML . 'ACP_header.html';

if(USER::$Data['Rank'] > $MaxRank):
	echo '<script type="text/javascript">alert("The max rank is ' . $MaxRank . '");</script>';
endif;
?>
		<button onclick="NewDialog('Información', 'Estás usando aXDR CMS 2, cualquier bug, comentalo aquí en los comentarios. <br /><br /> Información: aXDR CMS 2 d:20140916:1618', '', '<button onclick=\'CloseDialog()\'>Cerrar</button>')">Información</button>
		<div id="contentRotator" data-interval="10">
<?php
/*
$a = [['http://xdr.holobox.com.es/aXDR/Images/new.0.jpg', '', 'Tabl&oacute;n de noticias oficial', 'Aqu&iacute; se mostrar&aacute;n noticias oficiales de aXDR.', ''
	]
];

echo base64_encode(json_encode($a));
*/

$c = 0;

if(file_exists(KERNEL . '/Cache/' . 'ACP.Articles.es_ES.html') && ((time() - filemtime(KERNEL . '/Cache/' . 'ACP.Articles.es_ES.html')) < 86400)):
	$t = file_get_contents(KERNEL . '/Cache/' . 'ACP.Articles.es_ES.html');
endif;

$c;
$url = '';

if(!isset($t) && get_http_response_code($url) == "200"):
	$t = base64_decode(file_get_contents($url));
	$_file = fopen(KERNEL . '/Cache/' . 'ACP.Articles.es_ES.html', 'w');
	fwrite($_file, $t);
	fclose($_file);
elseif(!isset($t) && get_http_response_code($url) != "200"):
	$t = file_get_contents(KERNEL . '/Cache/' . 'ACP.Articles.es_ES.html');
endif;


if(strlen($t) > 15 && strstr($t, '[') !== FALSE):
	$a = json_decode($t, true); //Will Return NULL is Special Characters are in the way...
	foreach($a as $n):
	
		if(count($n) !== 5)
			continue;

		$c++;
?>
          <div id="content_<?php echo $c; ?>" class="rotatorItem<?php echo ($c === 1) ? ' rotatorCurrent' : ''; ?>" style="background-image: url('<?php echo $n["background"]; ?>');">
            <h2>
              <a href="http://<?php echo $n["link-url"]; ?>"><?php echo $n["title"]; ?></a>
            </h2>
            <h3><?php echo $n["description"]; ?></h3>
            <?php if(!empty($n[1])): ?><a href="<?php echo $n["link-url"]; ?>"><?php echo $n["button-message"]; ?><div class="ButtonNext" style="float: right;margin-left:8px;"></div></a><?php endif; ?>
          </div>
<?php
	endforeach;
endif;

echo '		<div id="rotatorThumbs">';
while($c > 0):
	$r = ($c === 1) ? 'On' : 'Off';
	echo '<div id="thumb_' . $c . '" style="" class="rotatorThumb rotatorThumb' . $r . '" tabindex="0" onclick="changeRotator(' . $c . ')"></div>';
	$c--;
endwhile;
?>
			</div>
		</div>
		<br />
		
		<div class="clearfix"></div>
		<div style="width: 350px; height: 150px; overflow-y: scroll; float:left">
			<h2>Tus logs (<a href="<?php echo HPATH;?>/manage?p=users&filter=<?php echo bin2hex('<!-- type:logs -->' . USER::$Data['ID']); ?>">Ver más</a>)</h2>
			<table class="striped">
<?php
$logsQuery = $MySQLi->query('SELECT timestamp, note FROM xdrcms_staff_log WHERE userid = ' . USER::$Data['ID'] . ' ORDER BY id DESC LIMIT 5');
while($Row = $logsQuery->fetch_assoc()):
?>
				<tr>
					<td><?php echo METHOD::ParseUNIXTIME($Row['timestamp']); ?></td>
					<td><?php echo $Row['note']; ?></td>
				</tr>
<?php endwhile; ?>
			</table>
		</div>
		<div style="padding-left: 10px; width: 350px; height: 150px; overflow-y: scroll;  float:left">
			<h2>Últimos logs (<a href="<?php echo HPATH; ?>/manage?p=logs">Ver más</a>)</h2>
			<table class="striped">
<?php
$logsQuery = $MySQLi->query('SELECT timestamp, note FROM xdrcms_staff_log ORDER BY id DESC LIMIT 5');
while($Row = $logsQuery->fetch_assoc()):
?>
				<tr>
					<td><?php echo METHOD::ParseUNIXTIME($Row['timestamp']); ?></td>
					<td><?php echo $Row['note']; ?></td>
				</tr>
<?php endwhile; ?>
			</table>
		</div>
		<div id="clearfix"></div>
		<a>Tablón de comentarios</a>
		<ul class="listview image fluid">
<?php
$query = $MySQLi->query('SELECT xdrcms_acp_comments.Title, xdrcms_acp_comments.Body, xdrcms_acp_comments.Created, users.username, users.look FROM xdrcms_acp_comments, users WHERE xdrcms_acp_comments.userID = users.id ORDER BY xdrcms_acp_comments.ID DESC LIMIT 12');
while($Row = $query->fetch_assoc()):
?>
			<li>
				<div class="icon" style="border: 0">
					<img src="<?php echo LOOK; echo $Row['look']; ?>&size=b&direction=3&head_direction=2">
				</div>
				<div class="data">
					<h4><?php echo $Row['Title']; ?></h4>
					<p><?php echo $Row['Body']; ?></p>
					<a><?php echo METHOD::ParseUNIXTIME($Row['Created']); ?></a>
				</div>
				<div class="badge"><?php echo $Row['username']; ?></div>
			</li>
<?php endwhile; ?>
		</ul>

		<ul class="accordion dark" id="accordionid">
			<li class="accordionli">
				<a onclick="ChangeAccordion(this)" href="javascript:void(0)"><span></span>Comentar</a>
				<div class="accordionccn">
					<form action='<?php echo HPATH; ?>/manage?p=dashboard&do=new' method='post' name='theAdminForm' id='theAdminForm' class="block-content form">
						<input id="saveButton" type="hidden" value=""></input>
						<?php echo getAntiCsrf(); ?>
						<table width='100%' cellpadding='5' align='left' border='0'>
							<tr>
								<td class='tablerow1' width='20%'  valign='middle'><b>Título</b>
									<div class='graytext'>
									</div>
								</td>
								<td class='tablerow2' width='40%'  valign='middle'>
									<input type='text' name='title' id='title' value="" size='30' class='textinput'>
								</td>
							</tr>
							<tr>
								<td class='tablerow1' width='20%'  valign='middle'><b>Contenido</b>
									<div class='graytext'>
									</div>
								</td>
								<td class='tablerow2' width='40%'  valign='middle'>
									<textarea name='body' cols='150' rows='3' wrap='soft' id='sub_desc' class='multitext'></textarea>
								</td>
							</tr>
							<tr>
								<td align='right' class='tablesubheader' colspan='2' >
									<input class='realbutton' type="submit" onclick="submitted=true" value="Crear" accesskey='s'/>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</li>
		</ul>
    <script async type="text/javascript">
        window.onload=function(){
<?php if($GLOBALS["Restrictions"]["Security"]["SecretKeys"]["Enabled"] && !checkSecurityKey(USER::$Data['ID'], $_SESSION["Manage"]["SecretKey"]) && !isset($_SESSION["Manage"]["AlertSecret"])): ?>
			NewDialog('Security', 'Has ingresado una Secret Key inválida. Puedes navegar por la administración pero no puedes hacer cambios. <br /><br /> Si quieres hacer cambios, porfavor, conéctate: ', '<div style="overflow:hidden"><img src="<?php echo LOOK . USER::$Data['Look']; ?>&size=s" style="float:left"></img><h3><?php echo USER::$Data['Name']; ?></h3><div style="margin:null; margin-bottom: 5px;color: white;"><?php echo USER::$Data['Email']; ?></div><p></p><div style="position: relative; width: 100%;"><input type="password" name="secretKey" id="i0118" maxlength="5" style="width: 17.5em;" placeholder="Secret Key"></input></div></div>', '<button onclick=\'LoginSC()\' class=\'marked\'>Conectar</button><button onclick=\'CloseDialog()\'>Cerrar</button>');
<?php endif; ?>
		}
	</script>
	<div title="It's the best." style="position: fixed;padding: 0px;right: 0px;bottom: 0px">¡Arrodíllense ante Madara Uchiha!</div>
