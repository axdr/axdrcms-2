<?php
if(file_exists(KERNEL . '/Cache/' . 'ACP.Help.es_ES.html')):
	if((time() - filemtime(KERNEL . '/Cache/' . 'ACP.Help.es_ES.html')) < 86400):
		$t = file_get_contents(KERNEL . '/Cache/' . 'ACP.Help.es_ES.html');
	endif;
endif;

if(!isset($t)):
	$t = file_get_contents('http://xdr.holobox.com.es/aXDR/ACP.Help.es_ES.html');
	$t = strstr($t, 'aXDR') !== FALSE ? $t : '';

	$_file = fopen(KERNEL . '/Cache/' . 'ACP.Help.es_ES.html', 'w');
	fwrite($_file, $t);
	fclose($_file);
endif;


$PageName = 'Ayuda';
require HTML . 'ACP_header.html';

if(strlen($t) > 500):
	echo METHOD::TAGS(str_replace(['%HPATH%'], [HPATH], $t));
else: ?>
<div style="display: block;">
	<div class="box">
		<div class="header">
			aXDR
		</div>
		<div class="body">
			aXDR or azureXDR it's a CMS created by <b>Xdr</b>. <br/><span style="color:red">This is not responsible for the use that you are given.</span>
		</div>
	</div>
	<div class="box">
		<div class="header blue">
			Configure Page Client
		</div>
		<div class="body">
			You can configure the client by going to <a href="<?php echo HPATH; ?>/manage?p=loader">SWF's</a> <br /><br />
			You can configure the SWFs by two ways, importing the SWF or configuring the SWF by yourself. <br /><br />
			<b>Importing: </b> Get a <i>json</i> code generated from the same page of one of your friends, or from prehostswf and click Import and introduce the code. Then save it and that's it. <br />
			<b>Configuring: </b> If you have your own SWF, do it manually, you will know how. Note: The optional fields, to load the default options from the variable, you have to leave them blank.
		</div>
	</div>
	<div class="box">
		<div class="header purple">
			Secret Key
		</div>
		<div class="body">
			The Secret Key it's a security system for users that have access to the ACP. The Secret Key it's actually a token, and with that it gets protection against csrf. <br /><br />
			To activate it, in the <b>User.Config.php</b>, put a "<b>true</b>" the option <b>$Restrictions['Security']['SecretKeys']['Enabled']</b>, and then, below in <b>['Keys'][ ... ]</b>, to each user with rank be adding <b>UserID => 'SecretKEY'</b><br />
			<b>Example:</b> [1 => '12345', 5 => 'ASD32']
			<br /><br />
			<span style="color:red">Important!</span> The SecretKEY has to have 5 characters obligatory.
		</div>
	</div>
	<div class="box">
		<div class="header darkblue">
			Bans
		</div>
		<div class="body">
			 You can view the bans by going to <a href="<?php echo HPATH; ?>/manage?p=users&filter=3c212d2d20747970653a62616e73202d2d3e">Users Manage</a> <br /><br />
			 Here you can find the banned users, see the ban details, and were you can unban the users.
			 <span style="color:red">Important!</span> When unbanning, the emulator still conserves the ban on its "cache", to unban you will need to reset the emulator too.
		</div>
	</div>
	<div class="box">
		<div class="header pink">
			Share your SWF easily. 
		</div>
		<div class="body">
		Go to <a href="<?php echo HPATH; ?>/manage?p=loader">SWF's</a> and click where it says "Export", the CMS is going to give you a code. That code is the one that you will share with your friends, and your friends have to "Import" it, into their SWF page.
		</div>
	</div>
	<div class="box">
		<div class="header orange">
		Manteminiento
		</div>
		<div class="body">
		Edit your 'User.Config' file, and where it says "Maintenance" change 'false' to 'true'.
		</div>
	</div>
	<div class="box">
		<div class="header">
         Time Zone
		</div>
		<div class="body">
		Go to 'KERNEL-XDRCMS/Init.php' and look for: <i>date_default_timezone_set('Europe/Madrid');</i> and change 'Europe/Madrid' to your timezone.
		</div>
	</div>
</div>
<div title="To save the information" style="position: fixed;padding: 0px;right: 0px;bottom: 0px">Thanks to KekoMundo</div>
<?php endif; ?>