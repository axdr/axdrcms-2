<?php
$vQueryCount = $MySQLi->query('SELECT COUNT(*) FROM ' . $Config['Vouchers']['TableName'])->fetch_assoc()['COUNT(*)'];

if($do === 'new' && isset($_POST['code'], $_POST['credits']) && is_numeric($_POST['credits'])):
	if(!checkAntiCsrf()):
		$msg_error = 'Invalid Secret Key.';
	elseif(strlen($_POST['code']) < 5):
		$msg_error = 'C�digo muy corto';
	elseif($_POST['credits'] > 5000000):
		$msg_error = 'Valor demasiado grande.';
	else:
		$c = $MySQLi->query('SELECT null FROM ' . $Config['Vouchers']['TableName'] . ' WHERE ' . $Config['Vouchers']['CodeColumn'] . ' = \'' . $_POST['code'] . '\' LIMIT 1'); 
		if($c && $c->num_rows === 1):
			$msg_error = 'Already exists this code.';
		else:
			$msg_correct = 'Created.';
			$MySQLi->query('INSERT INTO ' . $Config['Vouchers']['TableName'] . ' (' . $Config['Vouchers']['CodeColumn'] . ', ' . $Config['Vouchers']['ValueColumn'] . ') VALUES (\'' . $_POST['code'] . '\', \'' . $_POST['credits'] . '\')');
			$MySQLi->query('INSERT INTO xdrcms_staff_log (action,message,note,userid,targetid,timestamp) VALUES (\'ACP\',\'Created a voucher.\',\'vouchers.php\', ' . USER::$Data['ID'] . ', 0,' . time() .')');
		endif;
	endif;
endif;

if(isset($_POST['CodeD'])):
	if(checkAntiCsrf()):
		$MySQLi->multi_query('DELETE FROM ' . $Config['Vouchers']['TableName'] . ' WHERE ' . $Config['Vouchers']['CodeColumn'] . ' = \'' . $_POST['CodeD'] . '\' LIMIT 1;INSERT DELAYED INTO xdrcms_staff_log (action,message,note,userid,targetid,timestamp) VALUES (\'Manage\',\'Deleted a voucher.\',\'vouchers.php\',\'' . USER::$Data['ID'] . '\', 0,\'' . time() . '\')');
		echo 'Voucher deleted.';
	else:
		echo 'Invalid Secret Key. Please log in with a valid Secret Key.';
	endif;
	exit;
elseif(isset($_GET['filter'])):
	$Search = str_replace(['<', '>', '"', '\'', '\\'], ['&lt;', '&gt;', '&quot;', '&#39;', '&#92;'], hex2bin($_GET['filter']));

	$queryOptions = '';
	$orderOption = 'DESC';
	$limitOption = 15;
	$pageOption = 1;
 
	// SEARCH FILTER 0.4 Beta
	// CODED BY XDR
	
	if(strpos($Search, '&lt;!-- ') !== false && strpos($Search, ' --&gt;') !== false):
		$_s = explode(';', explode(' --&gt;', explode('&lt;!-- ', $Search)[1])[0] . ';');

		foreach($_s as $o):
			if(empty($o) || strpos($o, ':') === false)
				continue;

			$o = explode(':', $o);

			if($o[0] === 'page' && (is_numeric($o[1]) && $o[1] > 0)):
				$pageOption = $o[1];
			endif;
		endforeach;
		$Search = preg_replace('/\&lt;!--(.*?)\--\&gt;/', '', $Search);
	endif;

	$_Page = (($pageOption * $limitOption) - $limitOption);	

	$vQuery = $MySQLi->query('SELECT ' . $Config['Vouchers']['CodeColumn'] . ', ' . $Config['Vouchers']['ValueColumn'] . ' FROM ' . $Config['Vouchers']['TableName'] . ' LIMIT ' . $_Page . ',' . $limitOption . '');
	$DataHTML = '<input type="hidden" id="usersTotal" value="' . $vQueryCount . '" /><input type="hidden" id="nowPage" value="' . $pageOption . '" /><input type="hidden" id="resultCount" value="' . $limitOption . '" />';

	if($vQuery && $vQuery->num_rows > 0):
		while ($Row = $vQuery->fetch_assoc()):
			$DataHTML .= '<tr><td>' . $Row[$Config['Vouchers']['CodeColumn']] . '</td><td>' . $Row[$Config['Vouchers']['ValueColumn']] . '</td><td><a  href="javascript:void(0)" onclick="DeleteVoucher(\'' . $Row[$Config['Vouchers']['CodeColumn']] . '\')">Borrar</a></td></tr>';
		endwhile;
	else:
		$DataHTML .= 'No se han encontrado vouchers.';
	endif;

	if(isset($_POST['onlyTable'])):
		echo $DataHTML;
		exit;
	endif;
endif;

$PageName = 'Vouchers';
require HTML . 'ACP_header.html';
?>
	<table class="striped">
		<thead>
			<tr>
				<th>C�digo</th>
				<th>Creditos</th>
				<th>Acciones</th>
			</tr>
		</thead>

		<tbody id="resultTable">
<?php
if(!isset($DataHTML)): ?>
		<input type="hidden" id="usersTotal" value="<?php echo $vQueryCount; ?>" />
		<input type="hidden" id="resultCount" value="15" />
		<input type="hidden" id="nowPage" value="1" />
<?php
$vQuery = $MySQLi->query('SELECT ' . $Config['Vouchers']['CodeColumn'] . ', ' . $Config['Vouchers']['ValueColumn'] . ' FROM ' . $Config['Vouchers']['TableName'] . ' LIMIT 15');
if($vQuery && $vQuery->num_rows > 0):
	while ($Row = $vQuery->fetch_assoc()):
?>
			<tr>
				<td><?php echo $Row[$Config['Vouchers']['CodeColumn']]; ?></td>
				<td><?php echo $Row[$Config['Vouchers']['ValueColumn']]; ?></td>
				<td><a href="javascript:void(0)" onclick="DeleteVoucher('<?php echo $Row[$Config['Vouchers']['CodeColumn']]; ?>')">Borrar</a></td>
			</tr>
<?php endwhile; else: echo 'No hay vouchers.'; endif; else: echo $DataHTML; endif; ?>
		</tbody>

		<tfoot></tfoot>
	</table>
	<div style="text-align: center;">
		<button onclick="ChangePage('first')">&lt;&lt;</button>
		<button onclick="ChangePage('back')">&lt;</button>
		<button onclick="ChangePage('next')">&gt;</button>
		<button onclick="ChangePage('last')">&gt;&gt;</button>
	</div>
	
	<input type="hidden" name="SCH" id="i0120" value="<?php echo (isset($_GET['filter'])) ? hex2bin($_GET['filter']) : ''; ?>">
	<br />
	<br />
	<br />
	<br />
	<ul class="accordion dark" id="accordionid">
		<li class="accordionli">
			<a onclick="ChangeAccordion(this)" href="javascript:void(0)"><span></span>Crear un c�digo voucher.</a>
			<div class="accordionccn">
				<form action='<?php echo HPATH; ?>/manage?p=vouchers&do=new' method='post' name='theAdminForm' id='theAdminForm' class="block-content form">
					<?php echo getAntiCsrf(); ?>
					<table width='100%' cellpadding='5' align='left' border='0'>
						<tr>
							<td class='tablerow1' width='20%'  valign='middle'><b>C�digo</b>
								<div class='graytext'>
								</div>
							</td>
							<td class='tablerow2' width='40%'  valign='middle'>
								<input type='text' name='code' id='title' value="<?php echo METHOD::RANDOM(8); ?>" size='30' class='textinput'>
							</td>
						</tr>
						<tr>
							<td class='tablerow1' width='20%'  valign='middle'><b>Creditos</b>
								<div class='graytext'>
								</div>
							</td>
							<td class='tablerow2' width='40%'  valign='middle'>
								<input type='text' name='credits' id='title' value="" size='30' class='textinput'>
							</td>
						</tr>
						<tr>
							<td align='right' class='tablesubheader' colspan='2' >
								<input class='realbutton' type="submit" value="Crear" accesskey='s'/>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</li>
	</ul>

	<script type="text/javascript">
		var uRank = <?php echo USER::$Data['Rank']; ?>;

		function DeleteVoucher(userId){
			if (window.XMLHttpRequest)
			{
				xmlhttp=new XMLHttpRequest();
			}
			else
			{
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.open("POST", "<?php echo HPATH; ?>/manage?p=vouchers", false);
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("CodeD=" + userId + "&GUID=" + GUID + "&uid=" + uid);
			
			alert(xmlhttp.responseText);
		}

		function SCHclick(){
			var sValue = element('#i0120').value;
			window.history.pushState("", "", 'manage?p=vouchers&filter=' + b2h(sValue));

			element("#resultTable").innerHTML = get("<?php echo HPATH; ?>/manage?p=vouchers&filter=" + b2h(sValue), "POST", "onlyTable=true");
		}
	</script>