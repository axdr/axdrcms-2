<?php
$PageName = 'Client DEBUG';
require HTML . 'ACP_header.html';

if($do === 'start'):
	$_SESSION['aDEBUG'][0] = true;
	unset($_SESSION['aDEBUG'][1]);
elseif($do === 'stop'):
	$_SESSION['aDEBUG'][0] = false;
endif;
?>
<hr />
Client DEBUG es una herramienta que se encarga de registrar todos los logs, eventos y errores que va emitiendo el client a lo largo que esté abierto. Esta herramienta es útil para diagnosticar errores para luego poder repararlos.
<br />
<br />
<?php if(isset($_SESSION['aDEBUG'][0]) && $_SESSION['aDEBUG'][0] === true): ?>
<button onclick="window.location.href='<?php echo HPATH; ?>/manage?p=debug&do=stop'">Parar captura</button>
<hr />
Para la captura para poder ver los eventos.
<?php elseif(isset($_SESSION['aDEBUG'][1]) && count($_SESSION['aDEBUG'][1]) > 0): ?>
<button onclick="window.location.href='<?php echo HPATH; ?>/manage?p=debug&do=start'">Iniciar captura</button>
<hr />
<table class="striped">
	<thead>
		<tr>
			<th>Tipo</th>
			<th>Información</th>
		</tr>
	</thead>
	<tbody id="resultTable">
<?php foreach($_SESSION['aDEBUG'][1] as $Row): ?>
		<tr>
			<td><?php echo $Row[0]; ?></td>
			<td><?php echo $Row[1]; ?></td>
		</tr>
<?php endforeach; ?>
	</tbody>
</table>
<?php else: ?>
<button onclick="window.location.href='<?php echo HPATH; ?>/manage?p=debug&do=start'">Iniciar captura</button>
<hr />
No hay nada capturado.
<?php endif; ?>