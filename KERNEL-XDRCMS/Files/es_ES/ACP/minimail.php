<?php
$PageName = 'Minimail';

if($do === 'new' || ($do === 'edit' && is_numeric($key)) && isset($_POST['campaign_title'], $_POST['campaign_image'], $_POST['campaign_body'])):
	$q = 'SELECT users.id FROM users,xdrcms_users_data WHERE xdrcms_users_data.web_online > ' . (time() - 2678400) . ' AND xdrcms_users_data.id = users.id';
	$cD = ['title' => $_POST['campaign_title'],
		'image' => $_POST['campaign_image'],
		'body' => $_POST['campaign_body']
	];
	if(isset($_POST['campaign_filter']) && is_numeric($_POST['campaign_filter']))
		$cD['filter'] = $_POST['campaign_filter'];
	
	if(!checkAntiCsrf()):
		$msg_error = 'Invalid Secret Key. Please, log in with your secret key.';
	elseif(strlen($cD['title']) < 5):
		$msg_error = 'El título es muy corto.';
	elseif(strlen($cD['body']) < 20):
		$msg_error = 'Esto no es un juego, t�mate esto más enserio. Debe tener m�nimo 20 letras.';
	elseif(!filter_var($cD['image'], FILTER_VALIDATE_URL)):
		$msg_error = 'La imágen no es válida.';
	endif;

	if(isset($msg_error))
		goto esalt;

	if($do === 'edit'):
		$MySQLi->query('UPDATE xdrcms_minimail_campaigns SET Title = \'' . $cD['title'] . '\', Message = \'' . $cD['body'] . '\', Image = \'' . $cD['image'] . '\' WHERE Id = ' . $key);
		$msg_correct = 'Modificada la campaña ' . $cD['title'];
		SLOG('Change', 'Modificada la campaña ' . $cD['title'] . ', id: ' . $key, 'manage.php[minimail]', 0);
		unset($cD);
		
		goto esalt;
	elseif(!isset($cD['filter'])):
		$msg_error = 'Anti hack filter.';
		goto esalt;
	endif;

	if($cD['filter'] == 4 && isset($_POST['users_list'])):
		$_POST['users_list'] = str_replace(' ', '', $_POST['users_list']);
		$a = explode(',', $_POST['users_list']);

		foreach($a as $R):
			if(!is_numeric($R)):
				$msg_error = 'Anti hack filter.';
				goto esalt;
			endif;
		endforeach;
		
		$q .= ' AND users.id IN (' . $_POST['users_list'] . ')';
	elseif(isset($_POST['user_rank']) && is_numeric($_POST['user_rank']) && $_POST['user_rank'] > 0 && $_POST['user_rank'] <= $MaxRank):
		$q .= ' AND users.rank ' . (($cD['filter'] == 1) ? '> ' : (($cD['filter'] == 2) ? '< ' : '= ')) . $_POST['user_rank'];
	elseif($cD['filter'] != 0):
		$msg_error = 'Anti hack filter.';
		goto esalt;
	endif;

	$q = $MySQLi->query($q);
	if($q == null || $q->num_rows === 0):
		$msg_error = 'No se han encontrado usuarios';
		goto esalt;
	endif;
	
	$qC = $MySQLi->query('INSERT INTO xdrcms_minimail_campaigns (Title, Message, Image, Creator) VALUES (\'' . $cD['title'] . '\', \'' . $cD['body'] . '\', \'' . $cD['image'] . '\', ' . USER::$Data['ID'] . ')');
	if($qC == null || $MySQLi->affected_rows !== 1):
		$msg_error = 'Ha ocurrido un error creando la campa�a';
		goto esalt;
	endif;
	$qC = $MySQLi->insert_id;
	
	while($uR = $q->fetch_assoc())
		$MySQLi->query('INSERT INTO xdrcms_minimail (OwnerId, SenderId, ToIds, Title, Message, Created, RelatedId) VALUES (' . $uR['id'] . ', 0, 0, \'\', ' . $qC . ', ' . time() . ', 0)');

	$msg_correct = 'Se ha creado la campa�a y se ha enviado la campa�a a ' . $q->num_rows . ' usuarios';
	SLOG('Create', 'Creada campa�a ' . $cD['title'] . ', id asignada: ' . $qC . ' y enviada a ' . $q->num_rows . ' usuarios', 'manage.php[minimail]', 0);
	unset($cD);
elseif($do == 'remove' && is_numeric($key)):
	$MySQLi->query('DELETE FROM xdrcms_minimail_campaigns WHERE Id = ' . $key . (!USER::CAN('editdelete') ? ' AND Creator = ' . USER::$Data['ID'] : ''));
	if($MySQLi->affected_rows !== 1):
		$msg_error = 'No se ha borrado la campa�a.';
	else:
		$MySQLi->query('DELETE FROM xdrcms_minimail WHERE Message = ' . $key);
		$msg_correct = 'Se ha borrado la campa�a.';
		SLOG('Remove', 'Borrada una campa�a. Id: ' . $key, 'manage.php[minimail]', 0);
	endif;
elseif($do == 'edit' && is_numeric($key)):
	$cQ = $MySQLi->query('SELECT * FROM xdrcms_minimail_campaigns WHERE Id = ' . $key);
	$cR = $cQ->fetch_assoc();
	if($cR['Creator'] != USER::$Data['ID'] && !USER::CAN('editdelete'))
		unset($cR);
endif;

esalt:

if(isset($_GET['filter'])):
	$_GET['filter'] = hex2bin($_GET['filter']);
	$Search = str_replace('\\', '&#92;', htmlentities($_GET['filter'], ENT_HTML401 | ENT_QUOTES, METHOD::GetCharset($_GET['filter'])));

	$queryOptions = '';
	$orderOption = 'DESC';
	$limitOption = 15;
	$pageOption = 1;
	
	$tableName = 'users, xdrcms_minimail_campaigns';
	$getColumns = 'xdrcms_minimail_campaigns.Id, xdrcms_minimail_campaigns.Title, xdrcms_minimail_campaigns.Creator, users.username';
 
	// SEARCH FILTER 0.4 preBeta
	// CODED BY XDR
	
	if(strpos($Search, '&lt;!-- ') !== false && strpos($Search, ' --&gt;') !== false):
		$_s = explode(';', explode(' --&gt;', explode('&lt;!-- ', $Search)[1])[0] . ';');

		foreach($_s as $o):
			if(empty($o) || strpos($o, ':') === false)
				continue;

			$o = explode(':', $o);

			if($o[0] === 'page' && (is_numeric($o[1]) && $o[1] > 0))
				$pageOption = $o[1];
		endforeach;
		$Search = preg_replace('/\&lt;!--(.*?)\--\&gt;/', '', $Search);
	endif;

	$_Page = (($pageOption * $limitOption) - $limitOption);	
	
	$mCC = $MySQLi->query('SELECT COUNT(*) FROM ' . $tableName)->fetch_assoc()['COUNT(*)'];
	$mCQ = $MySQLi->query('SELECT ' . $getColumns . ' FROM ' . $tableName . ' WHERE id > \'0\' ' . $queryOptions . ' ORDER BY id ' . $orderOption . ' LIMIT ' . $_Page . ',' . $limitOption . '');
	$DataHTML = '<input type="hidden" id="usersTotal" value="' . $mCC . '" /><input type="hidden" id="nowPage" value="' . $pageOption . '" /><input type="hidden" id="resultCount" value="' . $limitOption . '" />';

	if($mCQ && $mCQ->num_rows > 0):
		while ($Row = $mCQ->fetch_assoc()):
			$time = (is_numeric($Row['timestamp'])) ? date('d-M-o G:i:s', $Row['timestamp']) : '--';
			$DataHTML .= '<tr><td>' . $Row['Id'] . '</td><td>' . $Row['Title'] . '</td><td>' . $Row['username'] . '</td><td>' . (($Row['Creator'] == USER::$Data['ID'] || USER::CAN('editdelete')) ? '<a href="' . HPATH . '/manage?p=minimail&do=edit&key=' . $Row['Id'] . '">Editar</a> || <a href="javascript:void(0)" onclick="CDelete(' . $Row['Id'] . ')">Borrar</a>' : '') . '</td></tr>';
		endwhile;
	else:
		$DataHTML .= 'No se han encontrado campa�as.';
	endif;

	if(isset($_POST['onlyTable'])):
		echo $DataHTML;
		exit;
	endif;
endif;

require HTML . 'ACP_header.html';
if(!isset($mCC))
	$mCC = $MySQLi->query('SELECT COUNT(*) FROM xdrcms_minimail_campaigns')->fetch_assoc()['COUNT(*)'];
?>
<script type="text/javascript">
	function MChange(id){
		var sI = element(id).selectedIndex;
		element("#cUsers").disabled = (sI < 4);
		element("#cUsers").style.display = (sI < 4) ? "none" : "inline";
		element("#cRank").disabled = (sI == 0 || sI == 4);
		element("#cRank").style.display = (sI == 0 || sI == 4) ? "none" : "inline";
	}
	
	function CDelete(id){
		NewDialog('Aviso', '�Est�s seguro de querer borrar la campa�a? �No puedes deshacer los cambios!', '', '<button class=\'red\'onclick=\'DeleteC(' + id + ')\'>Borrar</button><button onclick=\'CloseDialog()\'>Cerrar</button>');
	}
	
	function DeleteC(id){
		get("<?php echo HPATH; ?>/manage?p=minimail&do=remove&key=" + id, "POST", "");
		window.location.reload();
	}
</script>

<hr />
<b>AVISO:</b> Est� secci�n no sirve para enviar mensajes privados a un determinado usuario, más bien lo contrario, sirve para enviar una campa�a (publicidad, anuncios, eventos, etc..) a la mayor�a de usuarios del Hotel, as� que no utilizar mal esta herramienta.
<br /><b>AVISO2:</b> Aunque le deis enviar a todos los usuarios, solo se le enviar�n a los usuarios que se hayan conectado en el �ltimo mes.
<br /><b>AVISO3:</b> Si editas una campa�a, se les actualizar� a todos los usuarios que tengan el mensaje(no se les enviar� de nuevo) y si borras una campa�a, se les borrar� a todos los usuarios.
<hr />
<table class="striped">
	<thead>
		<tr>
			<th>Id</th>
			<th>Título</th>
			<th>Creador</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody id="resultTable">
<?php
if(!isset($DataHTML)): ?>
		<input type="hidden" id="usersTotal" value="<?php echo $mCC; ?>" />
		<input type="hidden" id="resultCount" value="15" />
		<input type="hidden" id="nowPage" value="1" />
<?php
$usersQuery = $MySQLi->query('SELECT xdrcms_minimail_campaigns.Id, xdrcms_minimail_campaigns.Title, xdrcms_minimail_campaigns.Creator, users.username FROM users, xdrcms_minimail_campaigns WHERE users.id = xdrcms_minimail_campaigns.Creator ORDER BY xdrcms_minimail_campaigns.Id DESC LIMIT 15');
if($usersQuery && $usersQuery->num_rows > 0):
	while ($Row = $usersQuery->fetch_assoc()):
?>
			<tr>
				<td><?php echo $Row['Id']; ?></td>
				<td><?php echo $Row['Title']; ?></td>
				<td><?php echo $Row['username']; ?></td>
				<td><?php if($Row['Creator'] == USER::$Data['ID'] || USER::CAN('editdelete')): ?><a href="<?php echo HPATH; ?>/manage?p=minimail&do=edit&key=<?php echo $Row['Id']; ?>">Editar</a> || <a href="javascript:void(0)" onclick="CDelete(<?php echo $Row['Id']; ?>)">Borrar</a><?php endif; ?></td>
			</tr>
<?php endwhile; else: echo 'No se han encontrado campa�as'; endif; else: echo $DataHTML; endif; ?>
	</tbody>
</table>

<ul class="accordion dark" id="accordionid" data-role="accordion">
    <li class="accordionli <?php echo (isset($cD)) ? 'active' : ''; ?>">
        <a onclick="ChangeAccordion(this)" href="javascript:void(0)"><span></span> Crear campa�a</a>
        <div class="accordionccn">
			<form action='<?php echo HPATH; ?>/manage?p=minimail&do=new' method='post' class="block-content form">
			<?php echo getAntiCsrf(); ?>
				<table width='100%' cellpadding='5' align='left' border='0'>
					<tr>
						<td class='tablerow1' width='10%'  valign='middle'><b>Título</b>
							<div class='graytext'>
							</div>
						</td>
						<td class='tablerow2' width='30%'  valign='middle'>
							<input type='text' name='campaign_title' value="<?php echo (isset($cD)) ? $cD['title'] : ''; ?>" size='30' class='textinput'>
						</td>
					</tr>
					<tr>
						<td class='tablerow1'  width='20%' valign='middle'><b>Filtro de usuarios</b></td>
						<td class='tablerow2' width='40%'  valign='middle'>
							<select id="mCFilter" name="campaign_filter" onchange="MChange('#mCFilter')">
								<option value='0'>Tod@s</option>
								<option value='1'>Tod@s con el rango mayor a</option>
								<option value='2'>Tod@s con el rango menor a</option>
								<option value='3'>Tod@s con el rango igual a</option>
								<option value='4'>Concretar usuarios</option>
							</select>
							<input id="cUsers" type='text' name='users_list' value="" placeholder="Poner los ids de los usuarios separados por comas. Ej: 10,32,1,5" size="100" class="textinput" disabled="true" style="display:none">
							<select id="cRank" name='user_rank' disabled="true" style="display:none">
<?php $n = 1; while($n < $MaxRank + 1): ?>
								<option id="ur.<?php echo $n; ?>" value='<?php echo $n; ?>'><?php echo ($n === 1)? $hotelName : (isset($staffRanks[$n]) ? $staffRanks[$n][0] : $n); ?></option>
<?php $n++; endwhile; ?>
							</select>
						</td>
					</tr>				
					<tr>
						<td class='tablerow1' width='20%'  valign='middle'><b>imágen</b>
							<div class='graytext'>
								<img src="<?php echo (isset($cD)) ? $cD['image'] : 'http://images.habbo.com/c_images/album3236/em_wbk_2.gif'; ?>" id="cImage" height="64" width="64"> 
							</div>
						</td>
						<td class='tablerow2' width='40%'  valign='middle'>
							<input type='text' name='campaign_image' id='cImage2' value="<?php echo (isset($cD)) ? $cD['image'] : 'http://images.habbo.com/c_images/album3236/em_wbk_2.gif'; ?>" size='100' class='textinput' onchange="ChangeImage('cImage', 'cImage2', '')">
						</td>
					</tr>
					<tr>
						<td class='tablerow1' width='20%'  valign='middle'><b>Mensaje</b>
							<div class='graytext'>
							</div>
						</td>
						<td class='tablerow2' width='40%'  valign='middle'>
							<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
							<script type="text/javascript">
								tinymce.init({
								selector: "textarea#bodyId",
								plugins: [
								"link fullscreen hr paste code"
								],
								toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
								autosave_ask_before_unload: false,
								max_height: 200,
								min_height: 160,
								height : 180
								});
							</script>
							<textarea id='bodyId' name="campaign_body" style="width:900px; height:300px"><?php echo (isset($cD)) ? $cD['body'] : ''; ?></textarea>
						</td>
					</tr>
					<tr>
						<td align='right' class='tablesubheader' colspan='2' >
							<input class='realbutton' type="submit" value="Enviar" accesskey='s'/>
						</td>
					</tr>
				</table>
<?php if(isset($cD)): ?>
				<script type="text/javascript">
					element("#mCFilter").selectedIndex = <?php echo $cD['filter']; ?>;
					MChange('#mCFilter');
<?php if(isset($_POST['user_rank']) && is_numeric($_POST['user_rank'])): ?>
					element("#cRank").selectedIndex = <?php echo $_POST['user_rank']-1; ?>;
<?php endif; ?>
				</script>
<?php endif; ?>
			</form>
        </div>
    </li>
	

<?php if(isset($cR)): ?>
    <li class="accordionli active">
        <a onclick="ChangeAccordion(this)" href="javascript:void(0)"><span></span> Editar la campa�a <?php echo $cR['Title']; ?></a>
        <div class="accordionccn">
			<form action='<?php echo HPATH; ?>/manage?p=minimail&do=edit&key=<?php echo $key; ?>' method='post' class="block-content form">
			<?php echo getAntiCsrf(); ?>
				<table width='100%' cellpadding='5' align='left' border='0'>
					<tr>
						<td class='tablerow1' width='10%'  valign='middle'><b>Título</b>
							<div class='graytext'>
							</div>
						</td>
						<td class='tablerow2' width='30%'  valign='middle'>
							<input type='text' name='campaign_title' value="<?php echo $cR['Title']; ?>" size='30' class='textinput'>
						</td>
					</tr>			
					<tr>
						<td class='tablerow1' width='20%'  valign='middle'><b>imágen</b>
							<div class='graytext'>
								<img src="<?php echo $cR['Image']; ?>" id="eImage" height="64" width="64"> 
							</div>
						</td>
						<td class='tablerow2' width='40%'  valign='middle'>
							<input type='text' name='campaign_image' id='eImage2' value="<?php echo $cR['Image']; ?>" size='100' class='textinput' onchange="ChangeImage('eImage', 'eImage2', '')">
						</td>
					</tr>
					<tr>
						<td class='tablerow1' width='20%'  valign='middle'><b>Mensaje</b>
							<div class='graytext'>
							</div>
						</td>
						<td class='tablerow2' width='40%'  valign='middle'>
							<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
							<script type="text/javascript">
								tinymce.init({
								selector: "textarea#bodyId2",
								plugins: [
								"link fullscreen hr paste code"
								],
								toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
								autosave_ask_before_unload: false,
								max_height: 200,
								min_height: 160,
								height : 180
								});
							</script>
							<textarea id='bodyId2' name="campaign_body" style="width:900px; height:300px"><?php echo $cR['Message']; ?></textarea>
						</td>
					</tr>
					<tr>
						<td align='right' class='tablesubheader' colspan='2' >
							<input class='realbutton' type="submit" value="Guardar" accesskey='s'/>
						</td>
					</tr>
				</table>
			</form>
        </div>
    </li>
<?php endif; ?>
</ul>