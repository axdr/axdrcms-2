<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

if (!defined('IN_AZURE')):
	header("Location:".PATH);
	exit;
endif;
?>

<!--[if lt IE 7]>
<script type="text/javascript">
Pngfix.doPngImageFix();
</script>
<![endif]-->
<div id="footer">
        <p class="footer-links"><a href="<?php echo PATH; ?>/papers/termsAndConditions" target="_new">Términos y Condiciones</a>  |  <a href="<?php echo PATH; ?>/papers/privacy" target="_new">Política de Privacidad</a>  |  <a href="<?php echo PATH; ?>/papers/cookies" target="_new">Política de Cookies</a></p>
        <p class="copyright">2014 aXDR 2.1. <?php echo www; ?> is not affiliated with, endorsed, sponsored, or specifically approved by Sulake Corporation Oy or its Affiliates. <br/>
		All the content belongs to Sulake Corporation Oy. All rights reserved.</p>
	<?php CACHE::AppendPluginsPosition(6); ?>
</div>    </div>
</div>
<script type="text/javascript">
    LandingPage.checkLoginButtonSetTimer();
</script>

<script type="text/javascript">
HabboView.run();
</script>
    
    
        

</body>
</html>
