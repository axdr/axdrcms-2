<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

if (!defined("IN_AZURE"))
{
	header("Location:".PATH);
	exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $charsetm; ?>">
	<title><?php echo $siteName; ?> - <?php if(isset($pagename)) echo $pagename; ?> </title>

<script type="text/javascript">
var andSoItBegins = (new Date()).getTime();
</script>
	<link rel="shortcut icon" href="<?php echo webgallery; ?>/v2/favicon.ico" type="image/vnd.microsoft.icon" />
	<link rel="alternate" type="application/rss+xml" title="<?php echo $hotelName; ?>: RSS" href="<?php echo PATH; ?>/articles/rss.xml" />
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/embed.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/embed.js" type="text/javascript"></script>

<link rel="stylesheet" href="/styles/local/es.css" type="text/css" />

<script type="text/javascript"> 
document.habboLoggedIn = <?php echo (USER::$LOGGED) ? 'true' : 'false'; ?>;
var habboName = <?php echo (USER::$LOGGED) ? '"' . USER::$Data['Name'] . '"' : 'null'; ?>;
var habboId = <?php echo (USER::$LOGGED) ? USER::$Data['ID'] : 'null'; ?>;
var habboReqPath = "<?php echo PATH; ?>";
var habboStaticFilePath = "<?php echo webgallery; ?>";
var habboImagerUrl = "<?php echo PATH; ?>/habbo-imaging/";
var habboPartner = "";
var habboDefaultClientPopupUrl = "<?php echo PATH; ?>/client";
window.name = "habboMain";
if (typeof HabboClient != "undefined") {
    HabboClient.windowName = "<?php echo (USER::$LOGGED) ? USER::$Row['client_token'] : 'client'; ?>";
    HabboClient.maximizeWindow = true;
}
</script>

<meta property="fb:app_id" content="<?php echo $_Facebook["App"]["ID"]; ?>" /> 

<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/common.css" type="text/css" />
<?php if(URI == "/identity/avatars" || URI == "/identity/add_avatar" || URI == "/identity/link_account"): ?>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/avatarselection.css" type="text/css" />
<?php else: ?>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/embeddedregistration.css" type="text/css" />
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/identitysettings.css" type="text/css" />
<?php endif; ?>
<?php if(URI == "/identity/password" || URI == "/identity/password_change"): ?>
    <script type="text/javascript" src="https://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
<?php endif; ?>


<meta name="description" content="<?php echo $siteName; ?>: haz amig@s, �nete a la diversi�n y date a conocer." />
<meta name="keywords" content="<?php echo strtolower($siteName); ?>, mundo, virtual, red social, gratis, comunidad, personaje, chat, online, adolescente, roleplaying, unirse, social, grupos, forums, seguro, jugar, juegos, amigos, adolescentes, raros, furni raros, coleccionable, crear, coleccionar, conectar, furni, muebles, mascotas, dise�o de salas, compartir, expresi�n, placas, pasar el rato, m�sica, celebridad, visitas de famosos, celebridades, juegos en l�nea, juegos multijugador, multijugador masivo" />

<!--[if IE 8]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/v2/styles/ie8.css" type="text/css" />
<![endif]-->
<!--[if lt IE 8]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/v2/styles/ie.css" type="text/css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/v2/styles/ie6.css" type="text/css" />
<script src="<? echo PATH; ?>/web-gallery/static/js/pngfix.js" type="text/javascript"></script>
<script type="text/javascript">
try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {}
</script>

<style type="text/css">
body { behavior: url(<?php echo PATH; ?>/web-gallery/js/csshover.htc); }
</style>
<![endif]-->
<meta name="build" content="<?php echo $_XDRBuild; ?>">
</head>

<body id="embedpage">
<div id="container">