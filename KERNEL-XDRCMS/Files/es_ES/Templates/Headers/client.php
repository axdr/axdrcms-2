<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright � 2011 Xdr.
|+=========================================================+
|| # Xdr 2011. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

if (!defined("IN_AZURE")):
	header("Location:".PATH);
	exit;
endif;

if($siteBlocked):
	header('Location:' . PATH . '/error/blocked');
	exit;
endif;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Habbo:  </title>

<script type="text/javascript">
var andSoItBegins = (new Date()).getTime();
</script>
<link rel="shortcut icon" href="<?php echo webgallery; ?>/v2/favicon.ico" type="image/vnd.microsoft.icon" />
<link rel="alternate" type="application/rss+xml" title="Habbo: RSS" href="http://www.habbo.es/articles/rss.xml" />
<meta name="csrf-token" content="c946055b76"/>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/common.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/libs2.js" type="text/javascript"></script>

<script src="<?php echo webgallery; ?>/static/js/visual.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/libs.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/common.js" type="text/javascript"></script>


<script type="text/javascript">
var ad_keywords = "siiiiiiiiiiiiiiiiiiiiiiiiiiiii,gender%3Am,age%3A18";
var ad_key_value = "kvage=18;kvgender=m;kvtags=siiiiiiiiiiiiiiiiiiiiiiiiiiiii";
</script>
<script type="text/javascript">
document.habboLoggedIn = true;
var habboName = <?php if(LOGGED != "null") { echo "\"".$name."\""; } else { echo "null"; } ?>;
var habboId = <?php if(LOGGED != "null") { echo $myrow['id']; } else { echo "null"; } ?>;
var facebookUser = <?php if($is_facebook == true || FACEBOOK_LOGGED_IN){ echo "true"; } else { echo "false"; } ?>;
var habboReqPath = "<?php echo PATH; ?>";
var habboStaticFilePath = "<?php echo webgallery; ?>";
var habboImagerUrl = "http://www.habbo.es/habbo-imaging/";
var habboPartner = "<?php if($is_facebook == true || FACEBOOK_LOGGED_IN){ echo "FBC"; } else { echo ""; } ?>";
var habboDefaultClientPopupUrl = "<?php echo PATH; ?>/client";
window.name = "<?php echo $myrow['client_token']; ?>";
if (typeof HabboClient != "undefined") {
    HabboClient.windowName = "<?php echo $myrow['client_token']; ?>";
    HabboClient.maximizeWindow = true;
}


</script>

<meta property="fb:app_id" content="157382664122" />

<noscript>
    <meta http-equiv="refresh" content="0;url=/client/nojs" />
</noscript>



<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/habboflashclient.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/habboflashclient.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/identity.js" type="text/javascript"></script>