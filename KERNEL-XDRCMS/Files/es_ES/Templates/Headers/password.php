<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $charsetm; ?>">
	<title><?php echo $siteName; ?> - <?php if(isset($pagename)) echo $pagename; ?> </title>

<script type="text/javascript">
var andSoItBegins = (new Date()).getTime();
</script>
<link rel="shortcut icon" href="<?php echo webgallery; ?>/v2/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="alternate" type="application/rss+xml" title="<?php echo $hotelName; ?>: RSS" href="<?php echo PATH; ?>/articles/rss.xml">
<meta name="csrf-token" content="b6a333d72e"/>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/process.css" type="text/css" />

<script src="<?php echo webgallery; ?>/static/js/libs2.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/visual.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/libs.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/common.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/fullcontent.js" type="text/javascript"></script>

<link rel="stylesheet" href="http://habbo.es/styles/local/es.css" type="text/css" />

<script src="http://habbo.es/js/local/es.js" type="text/javascript"></script>

<script type="text/javascript"> 
document.habboLoggedIn = <?php echo (USER::$LOGGED) ? 'true' : 'false'; ?>;
var habboName = <?php echo (USER::$LOGGED) ? '"' . USER::$Data['Name'] . '"' : 'null'; ?>;
var habboId = <?php echo (USER::$LOGGED) ? USER::$Data['ID'] : 'null'; ?>;
var habboReqPath = "<?php echo PATH; ?>";
var habboStaticFilePath = "<?php echo webgallery; ?>";
var habboImagerUrl = "<?php echo PATH; ?>/habbo-imaging/";
var habboPartner = "";
var habboDefaultClientPopupUrl = "<?php echo PATH; ?>/client";
window.name = "habboMain";
if (typeof HabboClient != "undefined") {
    HabboClient.windowName = "<?php echo (USER::$LOGGED) ? USER::$Row['client_token'] : 'client'; ?>";
    HabboClient.maximizeWindow = true;
}
</script>

<meta property="fb:app_id" content="<?php echo $_Facebook["App"]["ID"]; ?>"> 

<meta property="og:site_name" content="<?php echo $hotelName; ?> Hotel" />
<meta property="og:title" content="<?php echo $hotelName; ?>: Home" />
<meta property="og:url" content="<?php echo PATH; ?>" />
<meta property="og:image" content="<?php echo PATH; ?>/v2/images/facebook/app_habbo_hotel_image.gif" />
<meta property="og:locale" content="es_ES" />

<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/frontpage.css" type="text/css" />

<meta name="description" content="Check into the world's largest virtual hotel for FREE! Meet and make friends, play games, chat with others, create your avatar, design rooms and more..." />
<meta name="keywords" content="<?php echo strtolower($siteName); ?>, virtual, world, social network, free, community, avatar, chat, online, teen, roleplaying, join, social, groups, forums, safe, play, games, online, friends, teens, rares, rare furni, collecting, create, collect, connect, furni, furniture, pets, room design, sharing, expression, badges, hangout, music, celebrity, celebrity visits, celebrities, mmo, mmorpg, massively multiplayer" />

<!--[if IE 8]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie8.css" type="text/css" />
<![endif]-->
<!--[if lt IE 8]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie.css" type="text/css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie6.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/pngfix.js" type="text/javascript"></script>
<script type="text/javascript">
try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {}
</script>

<style type="text/css">
body { behavior: url(/js/csshover.htc); }
</style>
<![endif]-->
<meta name="build" content="<?php echo $_XDRBuild; ?>">
</head>
<body class="process-template black secure-page">