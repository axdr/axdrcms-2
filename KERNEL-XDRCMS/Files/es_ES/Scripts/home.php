<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$edit_mode = false;
$body_id = 'viewmode';
$canEdit = false;

if(isset($_GET['id']) && $_GET['id'] > 0 && is_numeric($_GET['id'])):
	$searchid = $_GET['id'];
	
	if($searchid != USER::$Data['ID']):
		$user_sql = $MySQLi->query('SELECT users.id, users.username, users.online, xdrcms_users_data.visibility, users.account_created, users.motto, users.look, xdrcms_users_data.homeBg FROM users, xdrcms_users_data WHERE users.id = ' . $searchid . ' AND xdrcms_users_data.id = users.id LIMIT 1');
		$user_exists = $user_sql->num_rows;

		if($user_exists != null && $user_exists == 1):
			$user_row = $user_sql->fetch_assoc();
			$pagename = $user_row['username'];
		endif;
	else:
		$user_exists = 1;
		$user_row = USER::$Row;
		$pagename = USER::$Data['Name'];
		$pageid = 'profile';
	endif;
elseif(isset($_GET['name']) && strlen($_GET['name']) > 2):
	$searchname = $_GET['name'];

	if($searchname != USER::$Data['Name']):
		$user_sql = $MySQLi->query('SELECT users.id, users.username, users.online, xdrcms_users_data.visibility, users.account_created, users.motto, users.look, xdrcms_users_data.homeBg FROM users, xdrcms_users_data WHERE users.username = \'' . $searchname . '\' AND xdrcms_users_data.id = users.id LIMIT 1');
		if($user_sql):
			$user_exists = $user_sql->num_rows;
		
			if($user_exists != null && $user_exists == 1):
				$user_row = $user_sql->fetch_assoc();
				$pagename = $user_row['username'];
			endif;
		endif;
	else:
		$user_exists = 1;
		$user_row = USER::$Row;
		$pagename = USER::$Data['Name'];
	endif;
endif;

if(!isset($user_row)):
	require HEADER . 'community.php';
	require HTML . 'Community_error.html';
	require FOOTER . 'community.php';
	exit;
else:
	if($user_row['visibility'] !== 'EVERYONE' && ($user_row['id'] !== USER::$Data['ID']) && USER::$Data['Rank'] < 4):
		require HEADER . 'community.php';
		require HTML . 'Community_homes_private.html';
		exit;
	endif;

	if($user_row['id'] == USER::$Data['ID']):
		if(isset($_SESSION['home_edit'])):
			$edit_mode = true;
			$body_id = 'editmode';
		endif;

		$canEdit = true;
	endif;
endif;


$textNotes = [
	'note.remember.security' => '¡Atención!<br />Publicar información personal sobre ti mismo o tus amigos, como direcciones, teléfonos o e-mail, o utilizar cualquier contenido que vaya en contra de la Manera ' . $hotelName . ', tendrá una consecuencia inmediata:<br />Tu nota será borrada para siempre.',
	'note.welcome' => '¡Bienvenido a tu nueva y flamante ' . $hotelName . ' Home! Aquí podrás dejar tu huella con un sinfín de curiosas etiquetas y de notas llenas de color. Para comenzar a crear tu ' . $hotelName . ' Home, puls aen el botón \'Editar\'',
	'note.myfriends' => '¿Y mis amigos?<br />Para añadir tu lista de conocidas a tu página haz click en Editar y mira en tus elementos del inventario. Despues de colocarlo puedes movr y mirar como queda. <br />¡Vamos!'
];
$pageid = 'profile';
?>