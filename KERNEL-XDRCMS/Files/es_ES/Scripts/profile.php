<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$pagename = 'Mi perfil';
$body_id = 'profile';
$pageid = 'settings';

$do = '';
$tab = 0;

USER::REDIRECT(1);

$Tab = isset($_GET['tab']) && is_numeric($_GET['tab']) ? $_GET['tab'] : 2;

if($Tab == 2 && isset($_POST['motto'], $_POST['tab'], $_POST['visibility'], $_POST['style']) && isset($_SERVER['HTTP_REFERER']) && ($_SERVER['HTTP_REFERER'] == PATH . '/profile' || $_SERVER['HTTP_REFERER'] == PATH . '/profile?tab=2' || $_SERVER['HTTP_REFERER'] == PATH . '/profile/profileupdate')):
	$Timer = new Timer('Profile2');

	if($Timer->IsExpired(1)):
		$Timer->Reload();
		
		USER::$Row['block_newfriends'] = isset($_POST['block_newfriends']) ? 0 : 1;
		USER::$Row['visibility'] = $_POST['visibility'] === 'EVERYONE' ? 'EVERYONE' : 'NOBODY';
		USER::$Data['Motto'] = METHOD::CheckSTR($_POST['motto']) ? substr($_POST['motto'],0,32) : '';

		if($_POST['style'] == -1 || !is_numeric($_POST['style']) || !isset($SiteSettings['Styles'][$_POST['style']]) || !$SiteSettings['Styles'][$_POST['style']]['Actived'])
			setcookie('customStyle', '', time() - 3600, '/');
		else
			setcookie('customStyle', $_POST['style'], time() + (3600*24*14), '/');

		$Addon['Query'] = $MySQLi->query('SELECT id FROM xdrcms_addons');

		$DataSave = [];
		while($Addon['Row'] = $Addon['Query']->fetch_assoc())
			$DataSave[$Addon['Row']['id']] = isset($_POST['addon_' . $Addon['Row']['id']]) && $_POST['addon_' . $Addon['Row']['id']] == 'true';

		USER::$Row['AddonData'] = json_encode($DataSave);
		$MySQLi->query("UPDATE xdrcms_users_data, users SET users.block_newfriends = '" . USER::$Row['block_newfriends'] . "', users.motto = '" . USER::$Data['Motto'] . "', xdrcms_users_data.AddonData = '" . USER::$Row['AddonData'] . "', xdrcms_users_data.visibility = '" . USER::$Row['visibility'] . "' WHERE users.id = " . USER::$Data['ID'] . " AND xdrcms_users_data.id = users.id");

		$result = '¡Perfil actualizado!';
	else:
		$result = 'Has grabado tus datos demasiado rápido. Por favor, tómate un minuto para que podamos registrarlos correctamente.';
		$error = '1';
	endif;
endif;
?>