<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright � 2012 Xdr.
|+=========================================================+
|| # Xdr 2012. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/
require_once('../includes/core.php');

if(LOGGED_IN == TRUE){ header("Location:".PATH); exit; }

if($_SESSION['quickregister']['step2'])
{
header("Location:".PATH."/quickregister/email_password");
} else if($_SESSION['quickregister']['step3']){
header("Location:".PATH."/quickregister/email_password");
} else {

}

$do = $_GET['do'];

if($do == "submit")
{
unset($_SESSION['register_errors']);

$day = FilterText($_POST['bean_day']);
$month = FilterText($_POST['bean_month']);
$year = FilterText($_POST['bean_year']);
$gender = FilterText($_POST['bean_gender']);

$_SESSION['bean_day'] = $day;
$_SESSION['bean_month'] = $month;
$_SESSION['bean_year'] = $year;
$_SESSION['bean_gender'] = $gender;

	if($day < 1 || $day > 31 || $month > 12 || $month < 1 || $year < 1900 || $year > 2000){
		$_SESSION['register_errors'] = $_SESSION['register_errors']."Please supply a valid birthdate <br />";
		$failure = true;
	}

if($failure == true){
header("Location:".PATH."/quickregister/age_gate");
}else{
		$_SESSION['quickregister']['step2'] = "focus";
header("Location:".PATH."/quickregister/email_password");
}

}

require_once('../templates/quickregister_subheader.php');
?>

<div id="main-container" class="login">

<div class="cbb">
    <div id="alert-container" class="rounded">
        <div id="alert-title" class="notice">
            Account already exists
        </div>
        <div id="alert-text">
            Trying to create a new Habbo account? Log in to your existing account and add new characters by going to your Account Settings / Identity Settings.
        </div>

    </div>
</div>

    <div id="title">
        Log in to Habbo
    </div>

    <div id="inner-container" class="clearfix">
        <form method="post" action="https://www.habbo.com/quickregister/login_submit" id="quickregister-login" method="post" onsubmit="Overlay.show(null,'Loading...');">
            <input type="hidden" name="emailOnlyLogin" value="true" />

            <div id="login-container" class="field-content clearfix">
                <div class="left">
                    <div class="field">
                        <div class="label" class="login-text">Email:</div>
                        <input type="text" id="login-username" name="credentials.username" value="xd-draker@hotmail.es"  />
                    </div>
                    <div class="field">
                        <div class="label" class="registration-text">Password:</div>

                        <input type="password" name="credentials.password" id="login-password" maxlength="32" value="" />
                    </div>
                    <div id="login-forgot-password">
                        <a href="https://www.habbo.com/account/password/forgot" id="forgot-password"><span>Forgot your password?</span></a>
                    </div>
                </div>
                <div class="right text-right">
                </div>

            </div>
            <input type="submit" style="position:absolute; margin: -1500px;"/>            
        </form>
    </div>

    <div id="select">
        <div class="button">
            <a id="proceed-button" href="#" class="area">Login</a>
            <span class="close"></span>

        </div>
   </div>
</div>

<script type="text/javascript">
        $("login-username").focus();

    if (!!$("back-link")) {
        Event.observe($("back-link"), "click", function() {
            Overlay.show(null,'Loading...');
        });
    }

    Event.observe($("proceed-button"), "click", function() {
        Overlay.show(null,'Loading...');
        $("quickregister-login").submit();
    });
</script>


<script src="https://ssl.google-analytics.com/ga.js" type="text/javascript"></script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-448325-2");
pageTracker._trackPageview();
</script>

<script type="text/javascript">
    HabboView.run();
</script>

</body>
</html>