<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright � 2011 Xdr.
|+=========================================================+
|| # Xdr 2011. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$bg = "";
$is_member = false;
$error = false;
$member_rank = 0;

if(isset($_GET['id']) && is_numeric($_GET['id'])):
	$check = $MySQLi->query("SELECT name, ownerid, bg, type, created, roomid, description, pane, badge FROM groups_details WHERE id = '".$_GET['id']."' LIMIT 1");

	if($check->num_rows > 0):
		$groupid = $_GET['id'];
		$pageid = "group";

		$groupdata = $check->fetch_assoc();

		$pagename = "Grupo: ".$groupdata['name'];
		$ownerid = $groupdata['ownerid'];
		$bg = $groupdata['bg'];

		$members = $MySQLi->query("SELECT COUNT(*) FROM groups_memberships WHERE groupid = '".$groupid."' AND is_pending = '0'")->num_rows;
		
		if(LOGGED != "null"):
			$check = $MySQLi->query("SELECT * FROM groups_memberships WHERE userid = '".$my_id."' AND groupid = '".$groupid."' AND is_pending = '0' LIMIT 1");
			$is_member = $check->num_rows;

			if($is_member > 0):
				$is_member = true;
				$my_membership = $check->fetch_assoc();
				$member_rank = $my_membership['member_rank'];
			endif;
		endif;
	else:
		$error = true;
	endif;
elseif(isset($_GET['name'])):
	$GroupName = $_GET['name'];
	$check_name = $MySQLi->query("SELECT * FROM groups_links WHERE short = '" . $GroupName . "' LIMIT 1");
	$exist_name = mysql_num_rows($check_name);
	
	if($exists_name > 0):
		$linkdata = $check->fetch_assoc();
		$check = $MySQLi->query("SELECT * FROM groups_details WHERE id = '" . $linkdata['groupid']."' LIMIT 1");
		$exists = mysql_num_rows($check);	

		if($exists > 0):
			$groupdata = $check->fetch_assoc();
			$groupid = $groupdata['id'];
			$pageid = str_replace(" ", "-", $GroupName);
			$group_name = true;

			$error = false;		

			$pagename = "Grupo: ".$groupdata['name']."";
			$ownerid = $groupdata['ownerid'];
			$bg = $groupdata['bg'];

			$members = mysql_evaluate("SELECT COUNT(*) FROM groups_memberships WHERE groupid = '".$groupid."' AND is_pending = '0'");

			$check = $MySQLi->query("SELECT * FROM groups_memberships WHERE userid = '".$my_id."' AND groupid = '".$groupid."' AND is_pending = '0' LIMIT 1");
			$is_member = $check->num_rows;

			if($is_member > 0 && LOGGED != "null"):
				$is_member = true;
				$my_membership = $check->fetch_assoc();
				$member_rank = $my_membership['member_rank'];
			else:
				$is_member = false;
			endif;
		else:
			$error = true;
		endif;
	else:
		$error = true;
	endif;
else:
	$error = true;
endif;

if($error):
	$pagename = "Página de Grupo no localizada";
	$pageid = "home";
	
	require HEADER . "community.php";
	require HTML . "Error_Groups_404.html";
	require FOOTER . "community.php";
	
	exit;
else:
	if(isset($_GET['do']) && $_GET['do'] == "edit" && LOGGED != "null")
	{
		if($is_member == true && $member_rank > 1)
		{
			unset($_SESSION["Ajax"]["Update"]);
			unset($_SESSION['home_edit']);

			$_SESSION['group_edit'] = true;
			$_SESSION['group_edit_id'] = $groupid;
			$_SESSION['user_group_edit_id'] = $my_id;

			$Users->restoreWaitingItems($my_id);

			header("location: ".PATH."/groups/".$groupid."/id"); exit;
		}
		else
		{	
			$_SESSION['group_edit'] = false;
			header("location: ".PATH."/groups/".$groupid."/id"); exit;
		}

	}

	if(isset($_GET['do']) && $_GET['do'] == "canceledit" && LOGGED != "null")
	{
		unset($_SESSION['group_edit']);
		unset($_SESSION['group_edit_id']);
		$Users->restoreWaitingItems($my_id);
		header("location: ".PATH."/groups/".$groupid."/id"); exit;
	}

	if(isset($_SESSION['group_edit']) && $_SESSION['group_edit'] == true && $is_member == true && $member_rank > 1)
	{
		$edit_mode = true;
		$body_id = "editmode";
	}
	else
	{
		$edit_mode = false;
		$body_id = "viewmode";
	}

	if($error == true)
	{
		$body_id = "home";
	}
	$viewtools = "	<div class=\"myhabbo-view-tools\">\n";


	if(LOGGED != "null" && !$is_member && $groupdata['type'] !== "2")
	{
		$viewtools = $viewtools . "<a href=\"".PATH."/habblet/ajax_joingroup?groupId=".$groupid."\" id=\"join-group-button\">";

		if($groupdata['type'] == "0" || $groupdata['type'] == "3")
			$viewtools = $viewtools . "Unirse al Grupo";
		else
			$viewtools = $viewtools . "Enviar petici�n";

		$viewtools = $viewtools . "</a>";
	}


	if(LOGGED != "null" && (isset($my_membership) && $my_membership['is_current'] !== "1") && $is_member)
		$viewtools = $viewtools . "<a href=\"#\" id=\"select-favorite-button\">Hacer favorito</a>\n";

	if(LOGGED != "null" && (isset($my_membership) && $my_membership['is_current'] == "1") && $is_member)
		$viewtools = $viewtools . "<a href=\"#\" id=\"deselect-favorite-button\">Remover favorito</a>";

	if(LOGGED != "null" && $is_member && $my_id !== $ownerid)
		$viewtools = $viewtools . "<a href=\"".PATH."/habblet/ajax_leavegroup?groupId=".$groupid."\" id=\"leave-group-button\">Dejar el Grupo</a>\n";

	if(LOGGED != "null" && $my_id !== $ownerid)
		$viewtools = $viewtools . "<a href=\"#\" id=\"reporting-button\" style=\"display: none;\">Mostrar la opci�n de Informar.</a>\n<a href=\"#\" id=\"stop-reporting-button\" style=\"display: none;\">Ocultar la opci�n de Informar.</a>";

	$viewtools = $viewtools . "	</div>\n";
	
	require HEADER . "community.php";
	require HTML . "Community_groups.html";
endif;
?>