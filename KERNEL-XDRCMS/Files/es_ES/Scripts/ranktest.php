<?php
$MinRefers = 15;
CACHE::SetMinRefersForRank($MinRefers);

if(isset($_POST['testText'])):
	$_POST['testText'] = $_POST['testText'];

	if(strlen($_POST['testText']) < 100):
		echo 'El texto es muy corto. Tiene que tener m�nimo 100 car�cteres.';
	elseif(strlen($_POST['testText']) > 5000):
		echo 'El texto es muy largo. Tiene que tener m�ximo 5000 car�cteres.';
	elseif(!METHOD::CheckSTR($_POST['testText'])):
		echo 'Car�cteres inv�lidos.';
	elseif($MySQLi->query('SELECT null FROM xdrcms_users_testrank WHERE UserId = ' . USER::$Data['ID'])->num_rows > 2):
		echo 'No puedes enviar más peticiones.';
	elseif($MySQLi->query('SELECT null FROM xdrcms_users_testrank WHERE UserId = ' . USER::$Data['ID'] . ' AND Status = \'0\' LIMIT 3')->num_rows != 0):
		echo 'Ya has enviado una petici�n.';
	elseif(USER::GetRefersCount(USER::$Data['ID']) < $MinRefers):
		echo 'No tienes los referidos necesarios.';
	else:
		$MySQLi->query('INSERT INTO xdrcms_users_testrank (UserId, Time, Content) VALUES (' . USER::$Data['ID'] . ', \'' . time() . '\', \'' . $_POST["testText"] . '\')');
		echo 'Enviado con �xito.';
	endif;
	exit;
endif;
?>