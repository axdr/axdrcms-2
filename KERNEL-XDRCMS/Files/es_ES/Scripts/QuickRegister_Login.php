<?php
$Error = False;
	
if(isset($_POST["emailOnlyLogin"], $_POST["next"], $_POST["credentials_username"], $_POST["credentials_password"])):
	$UserName = $_POST["credentials_username"];
	$PassWord = HoloHash($_POST["credentials_password"]);
	
	if(empty($UserName) && empty($_POST["credentials_password"])):
		$ErrorName = "Por favor, introduce tu email y contraseña para conectarte.";
		$EmailError = true;
		$Error = True;
	elseif(empty($UserName)):
		$ErrorName = "Por favor, escribe tu email";
		$EmailError = true;
		$Error = True;
	elseif(empty($_POST["credentials_password"])):
		$ErrorName = "Por favor, escribe tu contraseña";
		$PassWordError = true;
		$Error = True;
	else:
		$Check = $Users->CheckData($UserName, $PassWord);
		
		if(!$Users->IsEmail($UserName)):
			$ErrorName = 'Tu contraseña y email no coinciden. Para leer más sobre problemas de conexi�n, por favor haz clic <a href="https://help.habbo.es/entries/368047-problemas-con-mi-habbo-id">aqu�.</a>.';
			$EmailError = true;
			$Error = True;
		elseif(!$Check):
			$ErrorName = 'Tu contraseña y email no coinciden. Para leer más sobre problemas de conexi�n, por favor haz clic <a href="https://help.habbo.es/entries/368047-problemas-con-mi-habbo-id">aqu�.</a>.';
			$EmailError = true;
			$Error = True;
		elseif($Users->IsEmail($Check)):
			$UserName = $Check;

			if(!$Users->CheckData($UserName, $PassWord)):
				$ErrorName = 'Tu contraseña y email no coinciden. Para leer más sobre problemas de conexi�n, por favor haz clic <a href="https://help.habbo.es/entries/368047-problemas-con-mi-habbo-id">aqu�.</a>.';
				$EmailError = true;
				$Error = True;
			endif;
		endif;
	endif;
	
	if(!$Error):
		$Users->CreateNewSession($UserName, $PassWord, "habboid", "");
		header ("Location: " . $_POST["next"]);
	endif;
endif;
?>