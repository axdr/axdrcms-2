<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2013 Xdr.
|+=========================================================+
|| # Xdr 2013. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/
unset($_SESSION['quickregister']['register_errors']);

if(!isset($_SESSION['quickregister']['USER']['Step2'])):
	header("Location:".PATH."/quickregister/email_password");
	exit();
endif;

if(!isset($_POST['captchaResponse'], $_POST["recaptcha_challenge_field"], $_SERVER["REMOTE_ADDR"])):
	$_SESSION['quickregister']['register_errors'] = "Codigo de seguridad no valido";
	header("Location:".PATH."/quickregister/captcha/e/12x");
	exit;
endif;

$failure = false;

$rConfig = GetAIOConfig("Register");
if(!$rConfig["register_enabled"]):
	echo "Registro no disponible";
	exit;
elseif($Users->IPRegistered(MY_IP, $rConfig)):
	echo "Ya tienes una cuenta creada.";
	exit;
endif;

$Step1 = $_SESSION['quickregister']['USER']['Step1'];
$Step2 = $_SESSION['quickregister']['USER']['Step2'];
$figure = "";

if(!$cConfig["client.new.user.reception"] && isset($_POST["bean_figure"])):
	$figure = $_POST["bean_figure"];
endif;

$captchaResponse = $_POST['captchaResponse'];

if (!$Users->CheckCaptcha($captchaResponse, $_POST["recaptcha_challenge_field"], $_SERVER["REMOTE_ADDR"])):
	$_SESSION['quickregister']['register_errors'] = "Codigo de seguridad no valido";
	$failure = true;
endif;

if(isset($_SESSION['registertemp'])):
	$web_register_user = time() - $_SESSION['registertemp']; // Segundos pasados desde que te registraste
	if($web_online_user > 17): // 18 Seg
		$failure = true;
	endif;
endif;

if(isset($_SESSION['accountes']) && $_SESSION['accountes'] > 30):
	$failure = true;
endif;

$useripcretes = $MySQLi->query("SELECT COUNT(*) FROM users WHERE ip_last = '" . MY_IP . "'")->num_rows;

if($useripcretes > 30):
	$failure = true;
endif;

if($failure == true):
	header("Location:".PATH."/quickregister/captcha/e/12x");
else:
	if(!$cConfig["client.new.user.reception"]):
		$checkFigure = $MySQLi->query("SELECT null FROM xdrcms_looks WHERE Look = '" . $figure . "' LIMIT 1");
		if($checkFigure->num_rows == 0):
			header("Location:".PATH."/quickregister/captcha/e/12x");
			exit;
		endif;
	endif;

	$Error1 = $Users->Step1($Step1[0], $Step1[1], $Step1[2], $Step1[3]);
	$Error2 = $Users->Step2($Step2[4], $Step2[0], $Step2[1], $Step2[2], $Step2[3]);

	if(!$Error1 || $Error2[0]):
		session_destroy();
		header("Location:" . PATH . "/quickregister/start");
		exit;
	endif;

	$scredits = $xdrcms_row["start_credits"];
	$spixels = $xdrcms_row["start_pixels"];
	$dob = $Step1[0] . "-" . $Step1[1] . "-" . $Step1[2];
	$password_hash = HoloHash($Step2[2]);
	$habboid = GenerateRandom("random_number", 10);

	$DataSave = "";

	$_IpExists = true;

	if($MySQLi->query("SELECT null FROM users WHERE (ip_last = '" . MY_IP . "' OR ip_reg  = '" . MY_IP . "')")->num_rows == 0):
		$_IpExists = false;
	endif;

	$MySQLi->query("INSERT INTO users (username, password, mail, birth, look, gender, rank, account_created, ip_last, postcount, credits, activity_points, last_online, newsletter, rpxid, rpx_type, referid, AddonData, RememberMeToken, AccountID, AccountIdentifier, AccountPhoto) VALUES ('" . $Step2[4] . "', '" . $password_hash . "', '" . $Step2[0] . "', '" . $dob . "', '" . $figure . "', '" .  $_SESSION['quickregister']['select_gender'] . "', '1', '" . time() . "', '" . MY_IP . "', '0', '" . $scredits . "', '" . $spixels . "', '--', '0', '" . $habboid . "', 'habboid', '0', '" . $DataSave . "', '', '', '', '')");
	$userid = $MySQLi->insert_id;

	if(isset($_SESSION["Register"]["RefId"]) && is_numeric($_SESSION["Register"]["RefId"]) && !$_IpExists):
		$Users::NewRefer($userid, $_SESSION["Register"]["RefId"]);
	endif;
	
	$Users->newItem($userid, 0, "180", "366", "11", "", "s_paper_clip_1", "", "sticker");
	$Users->newItem($userid, 0, "130", "22", "10", "", "s_needle_3", "", "sticker");
	$Users->newItem($userid, 0, "280", "343", "3", "", "s_sticker_spaceduck", "", "sticker");
	$Users->newItem($userid, 0, "593", "11", "9", "", "s_sticker_arrow_down", "", "sticker");

	$Users->newItem($userid, 0, "107", "402", "8", "", "n_skin_notepadskin", $System->CorrectStr("�Ehhh!, �y mis amigos?<br />Para a�adir tu lista de conocidos a la " . $hotelName . " Home, echa un vistazo a los elementos de tu Inventario. Una vez la coloques en la página, podr�s arrastrarla para situarla donde más te guste, e incluso podr�s cambiarle el \'look\'. �Haz l"), "stickie");
	$Users->newItem($userid, 0, "57", "229", "6", "", "n_skin_speechbubbleskin", $System->CorrectStr("�Bienvenido a tu nueva y flamante " . $hotelName . " Home!<br />Aqu� podr�s dejar tu huella con un sinf�n de curiosas etiquetas y de notas llenas de color. Para comenzar a crear tu " . $hotelName . " Home, pulsa en el bot�n \'Editar\'."), "stickie");
	$Users->newItem($userid, 0, "148", "41", "7", "", "n_skin_noteitskin", $System->CorrectStr("��Atenci�n!<br />Publicar informaci�n personal sobre ti mismo o tus amigos, como direcciones, tel�fonos o e-mails,  o utilizar cualquier contenido que vaya en contra de la Manera " . $hotelName . ", tendr� una consecuencia inmediata: �tu nota ser� borrada para siempre!"), "stickie");

	$Users->newItem($userid, 0, "457", "26", "4", "ProfileWidget", "w_skin_defaultskin", "", "widget");
	$Users->newItem($userid, 0, "450", "319", "1", "RoomsWidget", "w_skin_notepadskin", "", "widget");

		
	if($scredits > 0)
		newTransaction($userid, $date_full, $scredits, $System->CorrectStr("�Bienvenido a " . $hotelName . "!"));
	
	unset($_SESSION['quickregister']);

	$Users->CreateNewSession($Step2[0], $password_hash, "habboid", false);

	$_SESSION['registertemp'] = time();

	if(isset($_SESSION['accountes'])):
		$_SESSION['accountes'] = $_SESSION['accountes'] + 1;
	else:
		$_SESSION['accountes'] = 1;
	endif;

	$_SESSION['client_login'] = true;
	$_SESSION['next_login'] = true;

	if($xdrcms_row2["toClient"] == "true")
		header("Location: " . PATH . "/client");
	else
		header("Location: " . PATH . "/me");

	exit();
endif;
?>