<?php
spl_autoload_register(function ($class)
{
  $prefix = 'Facebook\\';
  $base_dir = defined('FACEBOOK_SDK_V4_SRC_DIR') ? FACEBOOK_SDK_V4_SRC_DIR : KERNEL . 'Other' . DS . '/Facebook/';

  $len = strlen($prefix);
  if (strncmp($prefix, $class, $len) !== 0)
    return;

  $relative_class = substr($class, $len);
  $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

  if (file_exists($file)) 
    require $file;
});
?>