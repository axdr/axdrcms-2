<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/


require '../KERNEL-XDRCMS/Init.php';

if($SiteSettings['staff.page.visibility'] === 1 && USER::$Data['Rank'] < $MinRank)
	exit(require 'error.php');

require SCRIPT . 'Staffs.php';
require HEADER . 'community.php';
require HTML . 'Community_staffs.html';
require FOOTER . 'community.php';
?>