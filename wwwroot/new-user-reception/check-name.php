<?php
// Coded by Xdr
ini_set('default_charset', 'ISO-8859-1');
header('Cache-Control: no-cache, no-store, must-revalidate');
header('Expires: Thu, 01 Jan 1970 00:00:00 GMT');
header('Content-Type: application/json;charset=UTF-8');
header('Pragma: no-cache');
header('P3P: CP="NON DSP COR CURa ADMa OUR STP STA"');
header('Connection: keep-alive');

if(isset($_POST['name'])):
	if(strlen($_POST['name']) < 3):
		echo '{"code":"INVALID_NAME","validationResult":{"resultType":"VALIDATION_ERROR_NAME_TOO_SHORT","additionalInfo":"2"},"suggestions":[]}';
	elseif(strlen($_POST['name']) > 15):
		echo '{"code":"INVALID_NAME","validationResult":{"resultType":"VALIDATION_ERROR_NAME_TOO_LONG","additionalInfo":"15"},"suggestions":[]}';
	elseif(strpos($_POST['name'], ' ')):
		echo '{"code":"INVALID_NAME","validationResult":{"resultType":"VALIDATION_ERROR_ILLEGAL_CHARS","additionalInfo":" "},"suggestions":[]}';
	elseif(strpos($_POST['name'], 'MOD-') !== false):
		echo '{"code":"INVALID_NAME","validationResult":{"resultType":"VALIDATION_ERROR_ILLEGAL_CHARS","additionalInfo":"MOD-"},"suggestions":[]}';
	elseif(preg_replace('/[^a-z\d\-=\?!@:\.]/i', '', $_POST['name']) !== $_POST['name']):
		echo '{"code":"INVALID_NAME","validationResult":{"resultType":"VALIDATION_ERROR_ILLEGAL_CHARS","additionalInfo":""},"suggestions":[]}';
	else:
		require '../../KERNEL-XDRCMS/Init.php';
		if(!USER::$LOGGED || USER::$Row['ReceptionPased'] == '1'):
			echo '{"code":"INVALID_NAME","validationResult":{"resultType":"VALIDATION_ERROR_NAME_TOO_SHORT","additionalInfo":"2"},"suggestions":[]}';
		elseif(strcasecmp($_POST['name'], USER::$Data['Name']) === 0):
			echo '{"code":"OK","suggestions":[]}';
		elseif(USER::SomeRegistered($_POST['name'], '', ['users', 'users.username'])):
			echo '{"code":"NAME_IN_USE","suggestions":["' . $_POST["name"] . 'ito","Monster' . $_POST["name"] . '","Guapo' . $_POST["name"] . '"]}';
		else:
			echo '{"code":"OK","suggestions":[]}';
		endif;
	endif;
	
	if (!defined('IN_AZURE')):
		ini_set('session.name', 'aXDR-RTM:1');
		session_start();
	endif;
	$_SESSION["newReceptionUserName"] = $_POST['name'];
else:
	echo '{"code":"INVALID_NAME","validationResult":{"resultType":"VALIDATION_ERROR_NAME_TOO_SHORT","additionalInfo":"2"},"suggestions":[]}';
endif;
?>