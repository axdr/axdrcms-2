<?php
// Coded by Xdr
require '../../KERNEL-XDRCMS/Init.php';

header('Cache-Control: no-cache, no-store, must-revalidate');
header('Expires: Thu, 01 Jan 1970 00:00:00 GMT');
header('Content-Type: application/json;charset=UTF-8');
header('Pragma: no-cache');
header('P3P: CP="NON DSP COR CURa ADMa OUR STP STA"');
header('Connection: keep-alive');

if(isset($_SERVER['HTTP_REFERER']) && parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) != www)
	exit();

if(!isset($_SESSION['newReceptionUserName'])):
	echo '{"code":"INVALID_NAME","validationResult":{"resultType":"VALIDATION_ERROR_NAME_TOO_SHORT","additionalInfo":"2"},"suggestions":[""]}';
	exit;
endif;

if(!USER::$LOGGED || USER::$Row['ReceptionPased'] == '1'):
	echo '{"code":"OK","suggestions":[]}';
elseif(strlen($_SESSION['newReceptionUserName']) < 3):
	echo '{"code":"INVALID_NAME","validationResult":{"resultType":"VALIDATION_ERROR_NAME_TOO_SHORT","additionalInfo":"2"},"suggestions":[]}';
elseif(strlen($_SESSION['newReceptionUserName']) > 15):
	echo '{"code":"INVALID_NAME","validationResult":{"resultType":"VALIDATION_ERROR_NAME_TOO_LONG","additionalInfo":"15"},"suggestions":[]}';
elseif(strpos($_SESSION['newReceptionUserName'], ' ')):
	echo '{"code":"INVALID_NAME","validationResult":{"resultType":"VALIDATION_ERROR_ILLEGAL_CHARS","additionalInfo":" "},"suggestions":[]}';
elseif(strpos($_SESSION['newReceptionUserName'], 'MOD-') !== false):
	echo '{"code":"INVALID_NAME","validationResult":{"resultType":"VALIDATION_ERROR_ILLEGAL_CHARS","additionalInfo":"MOD-"},"suggestions":[]}';
elseif(preg_replace('/[^a-z\d\-=\?!@:\.]/i', '', $_SESSION['newReceptionUserName']) !== $_SESSION['newReceptionUserName']):
	echo '{"code":"INVALID_NAME","validationResult":{"resultType":"VALIDATION_ERROR_ILLEGAL_CHARS","additionalInfo":""},"suggestions":[]}';
else:
	$Gender = isset($_SESSION['newReceptionGender']) ? $_SESSION['newReceptionGender'] : ((isset($_POST['isFemale']) || $_POST['isFemale'] == 'true') ? 'F' : 'M');
	$Look = isset($_SESSION['newReceptionLook']) ? $_SESSION['newReceptionLook'] : $MySQLi->query("SELECT Look FROM xdrcms_looks WHERE Gender = '" . $Gender . "' ORDER BY RAND() LIMIT 1")->fetch_assoc()['Look'];

	if(strcasecmp($_SESSION['newReceptionUserName'], USER::$Data['Name']) === 0):
		$MySQLi->query("UPDATE users, xdrcms_users_data SET users.gender = '" . $Gender . "', users.look = '" . $Look . "', xdrcms_users_data.ReceptionPased = '1' WHERE users.id = '" . USER::$Data['ID'] . "' AND xdrcms_users_data.id = users.id");
		echo '{"code":"OK","suggestions":[]}';
	elseif(USER::SomeRegistered($_SESSION["newReceptionUserName"], '', ['users', 'users.username'])):
		echo '{"code":"NAME_IN_USE","suggestions":["' . $_SESSION["newReceptionUserName"] . 'ito","Monster' . $_SESSION["newReceptionUserName"] . '","Guapo' . $_SESSION["newReceptionUserName"] . '"]}';
	else:
		$MySQLi->query("UPDATE users, xdrcms_users_data SET users.username = '" . $_SESSION["newReceptionUserName"] . "', users.gender = '" . $Gender . "', users.look = '" . $Look . "', xdrcms_users_data.ReceptionPased = '1' WHERE users.id = '" . USER::$Data['ID'] . "' AND xdrcms_users_data.id = users.id");
		USER::$Data['Name'] = $_SESSION["newReceptionUserName"];
		USER::$Data['Gender'] = $Gender;
		USER::$Data['Look'] = $Look;
		
		$MySQLi->query("UPDATE rooms SET owner = '" . $_SESSION["newReceptionUserName"] . "' WHERE owner = '" . USER::$Data['Name'] . "'");
		echo '{"code":"OK","suggestions":[]}';
	endif;
endif;
?>