<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

require "../../KERNEL-XDRCMS/Init.php";
if($siteBlocked):
	require HTML . "Error_403.html";
	exit;
endif;

header("Location: " . PATH . "/#registration");
?>
