<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

$no_maintenance = true;
$no_rea = true;

require "../KERNEL-XDRCMS/Init.php";

header("Content-type: text/xml"); 
?>
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.google.com/schemas/sitemap/0.84">
<?php
$Data["Query"] = $MySQLi->query("SELECT id FROM groups_details ORDER BY id DESC");

while($Data["Row"] = $Data["Query"]->fetch_assoc())
{
?>
<url><loc><?php echo PATH; ?>/groups/<?php echo $Data["Row"]["id"]; ?>/id</loc></url>
<?php } ?>
</urlset>