<?php
if(isset($_POST['error_desc'], $_POST['error_cat'])):
	ini_set('session.name', 'aXDR-RTM:1');
	@session_start();
	$_SESSION['clientError'] = [
		'Info' => $_POST['error_desc'],
		'ID' => $_POST['error_cat'],
		'Token' => rand(10000000, 99999999)
	];
	
	if(isset($_SESSION['aDEBUG'][0]) && $_SESSION['aDEBUG'][0] === true)
		$_SESSION['aDEBUG'][1][] = ['DEBUG FINAL ERROR', $_POST['debug']];
endif;
header('Location: /client_popup/flash_problem');
?>