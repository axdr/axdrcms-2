<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/
$no_rea = true;
require "../../KERNEL-XDRCMS/Init.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo $hotelName; ?>: <?php echo $siteName; ?> </title>

<script type="text/javascript">
var andSoItBegins = (new Date()).getTime();
</script>
<link rel="shortcut icon" href="<?php echo webgallery; ?>/v2/favicon.ico" type="image/vnd.microsoft.icon" />
<link rel="alternate" type="application/rss+xml" title="<?php echo $hotelName; ?>: RSS" href="<?php echo PATH; ?>/articles/rss.xml" />

<script src="<?php echo webgallery; ?>/static/js/libs2.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/prototype.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/libs.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/common.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/process.css" type="text/css" />
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/habboflashclient.css" type="text/css" />
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/v3_habblet.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/habboflashclient.js" type="text/javascript"></script>
<link href='//fonts.googleapis.com/css?family=Ubuntu:400,700,400italic,700italic|Ubuntu+Medium' rel='stylesheet' type='text/css'>

<script type="text/javascript"> 
document.habboLoggedIn = <?php echo (USER::$LOGGED) ? 'true' : 'false'; ?>;
var habboName = <?php echo (USER::$LOGGED) ? '"' . USER::$Data['Name'] . '"' : 'null'; ?>;
var habboId = <?php echo (USER::$LOGGED) ? USER::$Data['ID'] : 'null'; ?>;
var habboReqPath = "<?php echo PATH; ?>";
var habboStaticFilePath = "<?php echo webgallery; ?>";
var habboImagerUrl = "<?php echo PATH; ?>/habbo-imaging/";
var habboPartner = "";
var habboDefaultClientPopupUrl = "<?php echo PATH; ?>/client";
window.name = "<?php echo (USER::$LOGGED) ? USER::$Row['client_token'] : 'habboMain'; ?>";
if (typeof HabboClient != "undefined") {
    HabboClient.windowName = "<?php echo (USER::$LOGGED) ? USER::$Row['client_token'] : 'client'; ?>";
    HabboClient.maximizeWindow = true;
}
</script>

<meta property="fb:app_id" content="<?php echo $_Facebook['App']['ID']; ?>"> 

<meta property="og:site_name" content="<?php echo $hotelName; ?> Hotel" />
<meta property="og:title" content="<?php echo $hotelName; ?>: Home" />
<meta property="og:url" content="<?php echo PATH; ?>" />
<meta property="og:image" content="<?php echo PATH; ?>/v2/images/facebook/app_habbo_hotel_image.gif" />
<meta property="og:locale" content="es_ES" />

<meta name="description" content="<?php echo $siteName; ?>: haz amig@s, �nete a la diversi�n y date a conocer." />
<meta name="keywords" content="<?php echo strtolower($siteName); ?>, mundo, virtual, red social, gratis, comunidad, personaje, chat, online, adolescente, roleplaying, unirse, social, grupos, forums, seguro, jugar, juegos, amigos, adolescentes, raros, furni raros, coleccionable, crear, coleccionar, conectar, furni, muebles, mascotas, dise�o de salas, compartir, expresi�n, placas, pasar el rato, m�sica, celebridad, visitas de famosos, celebridades, juegos en l�nea, juegos multijugador, multijugador masivo" />


<!--[if IE 8]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie8.css" type="text/css" />
<![endif]-->
<!--[if lt IE 8]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie.css" type="text/css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie6.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/pngfix.js" type="text/javascript"></script>
<script type="text/javascript">
try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {}
</script>

<style type="text/css">
body { behavior: url(/js/csshover.htc); }
</style>
<![endif]-->
<meta name="build" content="<?php echo $_XDRBuild; ?>" />
</head>
<?php
require HTML . "Client_flash_problem.html";
?>