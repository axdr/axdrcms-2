<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

require '../../KERNEL-XDRCMS/Init.php';
USER::REDIRECT(1);

if(URI === '/identity/password_change' && isset($_POST['fromClient'], $_POST['currentPassword'], $_POST['newPassword'], $_POST['retypedNewPassword'], $_POST['recaptcha_response_field'], $_POST["recaptcha_challenge_field"], $_SERVER["REMOTE_ADDR"])):
	if(empty($_POST['newPassword']) || strlen($_POST['newPassword']) < 6 || strlen($_POST['newPassword']) > 30 || !preg_match('`[0-9]`', $_POST['newPassword'])):
		$Error = 'contraseña incorrecta. Por favor, introduce una contraseña válida.';
	elseif($_POST['newPassword'] !== $_POST['retypedNewPassword']):
		$Error = 'Tu nueva contraseña no se corresponde con la contraseña reescrita.';
	elseif(!METHOD::CheckCaptcha($_POST['recaptcha_response_field'], $_POST["recaptcha_challenge_field"], $_SERVER["REMOTE_ADDR"])):
		$Error = 'El c�digo de seguridad no era v�lido. Por favor, int�ntalo de nuevo.';
	elseif(METHOD::HASH($_POST['currentPassword']) !== $_SESSION['password']):
		$Error = 'Las contraseñas no coinciden.';
	else:
		$MySQLi->query('UPDATE xdrcms_users_data SET password = \'' . METHOD::HASH($_POST['newPassword']) . '\' WHERE id = ' . USER::$Data['ID']);
		$_SESSION['passwordChanged'] = true;
		header('Location: ' . PATH . '/identity/settings');
		exit;
	endif;
endif;

require 'password.php';
?>