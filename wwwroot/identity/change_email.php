<?php
if(!isset($_POST['__app_key'], $_POST['urlToken'], $_POST['email'], $_POST['currentPassword']))	exit;

require '../../KERNEL-XDRCMS/Init.php';
USER::REDIRECT(1);

if(USER::$Row['rpx_type'] !== 'habboid')	exit;

$response = [true, []];
if(empty($_POST['currentPassword'])):
	array_push($response[1], '[Falta tu contraseña. Introd�cela.]');
elseif($_SESSION['password'] !== METHOD::HASH($_POST['currentPassword'])):
	array_push($response[1], '[contraseña incorrecta]');
endif;

if(empty($_POST['email']) || strlen($_POST['email']) > 50 || preg_match("/^[a-z0-9_\.-]+@([a-z0-9]+([\-]+[a-z0-9]+)*\.)+[a-z]{2,7}$/i", $_POST['email']) !== 1):
	array_push($response[1], '[Por favor, introduce una direcci�n de email válida]');
elseif($MySQLi->query("SELECT NULL FROM xdrcms_users_data WHERE mail = '" . $_POST['email'] . "' AND rpx_type = 'habboid' LIMIT 1")->num_rows > 0):
	array_push($response[1], '[El email proporcionado ya est� siendo usado. Si intentas crear un nuevo personaje, por favor con�ctate con tu ' . $hotelName . ' cuenta y haz clic en A�adir personaje.]');
endif;

if($response[1] === []):
	if($MySQLi->query('UPDATE xdrcms_users_data SET mail = \'' . $_POST['email'] . '\' WHERE mail = \'' . $_SESSION['email'] . '\'')):
		$_SESSION['email'] = $_POST['email'];
		$email = $_POST['email'];
		$response = [false, []];
	else:
		array_push($response[1], '[Ha ocurrido un error.]');
	endif;
endif;

require 'email.php';
?>