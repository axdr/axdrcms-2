<?php
require '../../../KERNEL-XDRCMS/Init.php';
USER::REDIRECT(1);

if(isset($_GET['Id']) && is_numeric($_GET['Id'])):
	if(USER::$Data['ID'] == $_GET['Id']):
		unset($_SESSION['Ajax']['Update']);
		unset($_SESSION['group_edit']);
		unset($_SESSION['group_edit_id']);

		$_SESSION['home_edit'] = true;
		header('Location: ' . PATH . '/home/' . USER::$Data['Name']);
		exit;
	endif;
endif;

header('Location: ' . PATH);
exit;
?>