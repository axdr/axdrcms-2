<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2014 Xdr.
|+=========================================================+
|| # Xdr 2014. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

if(!isset($_POST['task'], $_POST['selectedId']) || !is_numeric($_POST['selectedId']))
	exit;

require '../../../KERNEL-XDRCMS/Init.php';
USER::REDIRECT(1);

$task = $_POST['task'];
$selectedId = $_POST['selectedId'];

$getItem = $MySQLi->query("SELECT price, amount, skin, type, ItemsContent FROM xdrcms_store_items WHERE id = '" . $selectedId . "' LIMIT 1"); 
if(!$getItem || $getItem->num_rows === 0)
	exit;

$row = $getItem->fetch_assoc();

if(USER::$Data['Credits'] < $row['price'])
	exit;

$newCredits = USER::$Data['Credits'] - $row['price'];
if($row['skin'] === 'package_product_pre'):
	$Items = explode(',', $row['ItemsContent']);

	foreach($Items as $ItemSkin) {
		$MySQLi->query("INSERT INTO xdrcms_site_inventory_items (userId, var, skin, type) VALUES ('" . USER::$Data['ID'] . "', '', '".$ItemSkin."', 'Sticker')");
	}
elseif($row['amount'] > 0):
	$count = 1;

	while($count <= $row['amount']) {
		$MySQLi->query("INSERT INTO xdrcms_site_inventory_items (userId, var, skin, type) VALUES (" . USER::$Data['ID'] . ", '', '".$row['skin']."', '".$row['type']."')");
		$count++;
	}
else:
	$MySQLi->query("INSERT INTO xdrcms_site_inventory_items (userId, var, skin, type) VALUES (" . USER::$Data['ID'] . ", '', '".$row['skin']."', '".$row['type']."')");
endif;

$MySQLi->query('UPDATE users SET credits = \'' . $newCredits . '\' WHERE id = ' . USER::$Data['ID']);
header('X-JSON: ' . $selectedId);
echo 'OK';
?>
