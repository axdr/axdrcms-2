<?php
/*=========================================================+
|| # Azure Files of XDRCMS. All rights reserved.
|| # Copyright ® 2013 Xdr.
|+=========================================================+
|| # Xdr 2013. The power of Proyects.
|| # Este es un Software de código libre, libre edición.
|+=========================================================+
*/

if(!isset($_POST['task'], $_POST['selectedId']) || !is_numeric($_POST['selectedId']))
	exit;

require '../../../KERNEL-XDRCMS/Init.php';
USER::REDIRECT(1);

$getItem = $MySQLi->query("SELECT price, amount, skin FROM xdrcms_store_items WHERE id = '" . $_POST['selectedId'] . "' AND type = 'WebCommodity' LIMIT 1"); 

if(!$getItem || $getItem->num_rows === 0)
	exit;

$row = $getItem->fetch_assoc();

if(USER::$Data['Credits'] < $row['price'])
	exit;

$newCredits = USER::$Data['Credits'] - $row['price'];
$count = 1;

while($count <= $row['amount']):
	$MySQLi->query("INSERT INTO xdrcms_site_inventory_items (userId, var, skin, type) VALUES ('" . USER::$Data['ID'] . "', '', '".$row['skin']."', 'WebCommodity')");
	$count++;
endwhile;

$MySQLi->query('UPDATE users SET credits = \'' . $newCredits . '\' WHERE id = ' . USER::$Data['ID']);
		
header('X-JSON: ' . $_POST['selectedId']);
echo 'OK';
?>