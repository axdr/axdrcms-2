<?php
if(!isset($_POST['ownerId'], $_POST['message'], $_POST['widgetId']) || !is_numeric($_POST['ownerId']) || !is_numeric($_POST['widgetId']))	exit;
require '../../../KERNEL-XDRCMS/Init.php';
USER::REDIRECT(1);

if(USER::$Data['ID'] !== $_POST['ownerId'])	exit;
?>
<ul class="guestbook-entries">
	<li id="guestbook-entry--1" class="guestbook-entry">
		<div class="guestbook-author">
                <img src="<?php echo LOOK . USER::$Data['Look']; ?>&direction=4&head_direction=4&gesture=sml&action=&size=s" alt="<?php echo USER::$Data['Name']; ?>" title="<?php echo USER::$Data['Name']; ?>"/>
		</div>
		<div class="guestbook-message">
			<div class="offline">
				<a href="<?php echo PATH; ?>/home/<?php echo USER::$Data['Name']; ?>"><?php echo USER::$Data['Name']; ?></a>
			</div>
			<p><?php echo METHOD::DecodeBBText($_POST['message']); ?></p>
		</div>
		<div class="guestbook-cleaner">&nbsp;</div>
		<div class="guestbook-entry-footer metadata"><?php echo date('d-M-o G:i:s');?></div>
	</li>
</ul>

<div class="guestbook-toolbar clearfix">
<a href="#" class="new-button" id="guestbook-form-continue"><b>Continuar editando</b><i></i></a>
<a href="#" class="new-button" id="guestbook-form-post"><b>A�adir mensaje</b><i></i></a>	
</div>