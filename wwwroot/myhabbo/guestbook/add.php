<?php
if(!isset($_POST['ownerId'], $_POST['message'], $_POST['widgetId']) || !is_numeric($_POST['ownerId']) || !is_numeric($_POST['widgetId']))	exit;
require '../../../KERNEL-XDRCMS/Init.php';
USER::REDIRECT(1);

if(USER::$Data['ID'] !== $_POST['ownerId'])	exit;

if(isset($_SESSION['guestBook']['lastEntry']) && ((time() - $_SESSION['guestBook']['lastEntry']) < 60)):
	echo 'You have to wait for a while before you can comment again.';
	exit;
endif;

$MySQLi->query('INSERT INTO xdrcms_guestbook (message, time, widget_id, userid) VALUES (\'' . $_POST['message'] . '\', \'' . time() . '\', \'' . $_POST['widgetId'] . '\', \'' . USER::$Data['ID'] . '\')');
$InsertId = $MySQLi->insert_id;

$_SESSION['guestBook']['lastEntry'] = time();
?>
	<li id="guestbook-entry-<?php echo $InsertId; ?>" class="guestbook-entry">
		<div class="guestbook-author">
                <img src="<?php echo LOOK . USER::$Data['Look']; ?>&direction=4&head_direction=4&gesture=sml&action=&size=s" alt="<?php echo USER::$Data['Name']; ?>" title="<?php echo USER::$Data['Name']; ?>"/>
		</div>
			<div class="guestbook-actions">
					<img src="<?php echo webgallery; ?>/images/myhabbo/buttons/delete_entry_button.gif" id="gbentry-delete-<?php echo $InsertId; ?>" class="gbentry-delete" style="cursor:pointer" alt=""/>
					<br/>
			</div>
		<div class="guestbook-message">
			<div class="<?php echo (USER::IsOnline(USER::$Data['ID'])) ? 'on' : 'off'; ?>line">
				<a href="<?php echo PATH; ?>/home/<?php echo USER::$Data['Name']; ?>"><?php echo USER::$Data['Name']; ?></a>
			</div>
			<p><?php echo METHOD::DecodeBBText($_POST['message']); ?></p>
		</div>
		<div class="guestbook-cleaner">&nbsp;</div>
		<div class="guestbook-entry-footer metadata"><?php echo date('d-M-o G:i:s');?></div>
	</li>