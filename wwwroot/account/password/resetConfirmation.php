<?php
require '../../../KERNEL-XDRCMS/FastInit.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo $hotelName; ?>: La petici�n se ha realizado con �xito </title>

<script type="text/javascript">
var andSoItBegins = (new Date()).getTime();
</script>
<link rel="shortcut icon" href="<?php echo PATH; ?><?php echo webgallery; ?>/v2/favicon.ico" type="image/vnd.microsoft.icon" />
<link rel="alternate" type="application/rss+xml" title="<?php echo $hotelName; ?>: RSS" href="<?php echo PATH; ?>/articles/rss.xml" />

<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/common.css" type="text/css" />
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/process.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/libs2.js" type="text/javascript"></script>

<script src="<?php echo webgallery; ?>/static/js/visual.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/libs.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/common.js" type="text/javascript"></script>
<script src="<?php echo webgallery; ?>/static/js/fullcontent.js" type="text/javascript"></script>
<link rel="stylesheet" href="/styles/local/secure.es.css" type="text/css" />

<script src="/js/local/secure.es.js" type="text/javascript"></script>

<script type="text/javascript">
var ad_keywords = "";
var ad_key_value = "";
</script>
<script type="text/javascript">
document.habboLoggedIn = false;
var habboName = null;
var habboId = null;
var habboReqPath = "";
var habboStaticFilePath = "<?php echo webgallery; ?>/";
var habboImagerUrl = "<?php echo PATH; ?>/habbo-imaging/";
var habboPartner = "";
var habboDefaultClientPopupUrl = "<?php echo PATH; ?>/client";
window.name = "habboMain";
if (typeof HabboClient != "undefined") {
    HabboClient.windowName = "client";
    HabboClient.maximizeWindow = true;
}


</script>

<meta property="fb:app_id" content="157382664122" />

<meta property="og:site_name" content="Habbo Hotel" />
<meta property="og:title" content="Habbo: La petici�n se ha realizado con �xito" />
<meta property="og:url" content="<?php echo PATH; ?>" />
<meta property="og:image" content="<?php echo PATH; ?>/v2/images/facebook/app_habbo_hotel_image.gif" />
<meta property="og:locale" content="es_ES" />



<meta name="description" content="Habbo Hotel: haz amig@s, �nete a la diversi�n y date a conocer." />
<meta name="keywords" content="habbo hotel, mundo, virtual, red social, gratis, comunidad, personaje, chat, online, adolescente, roleplaying, unirse, social, grupos, forums, seguro, jugar, juegos, amigos, adolescentes, raros, furni raros, coleccionable, crear, coleccionar, conectar, furni, muebles, mascotas, dise�o de salas, compartir, expresi�n, placas, pasar el rato, m�sica, celebridad, visitas de famosos, celebridades, juegos en l�nea, juegos multijugador, multijugador masivo" />



<!--[if IE 8]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie8.css" type="text/css" />
<![endif]-->

<!--[if lt IE 8]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie.css" type="text/css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" href="<?php echo webgallery; ?>/static/styles/ie6.css" type="text/css" />
<script src="<?php echo webgallery; ?>/static/js/pngfix.js" type="text/javascript"></script>
<script type="text/javascript">
try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {}
</script>

<style type="text/css">
body { behavior: url(/js/csshover.htc); }
</style>
<![endif]-->
<meta name="build" content="63-BUILD944 - 30.11.2011 12:59 - es" />
</head>
<body class="process-template secure-page">

<div id="overlay"></div>

<div id="change-password-form" style="display: none;">
    <div id="change-password-form-container" class="clearfix">
        <div id="change-password-form-title" class="bottom-border">�contraseña olvidada?</div>
        <div id="change-password-form-content" style="display: none;">
            <form method="post" action="<?php echo PATH; ?>/account/password/identityResetForm" id="forgotten-pw-form">

                <input type="hidden" name="page" value="/account/password/resetConfirmation?changePwd=true" />
                <span>Por favor, introduce el email de tu Habbo cuenta:</span>
                <div id="email" class="center bottom-border">
                    <input type="text" id="change-password-email-address" name="emailAddress" value="" class="email-address" maxlength="48"/>
                    <div id="change-password-error-container" class="error" style="display: none;">Por favor, introduce un e-mail</div>
                </div>
            </form>
            <div class="change-password-buttons">

                <a href="#" id="change-password-cancel-link">Cancelar</a>
                <a href="#" id="change-password-submit-button" class="new-button"><b>Enviar email</b><i></i></a>
            </div>
        </div>
        <div id="change-password-email-sent-notice" style="display: none;">
            <div class="bottom-border">
                <span>Te hemos enviado un email a tu direcci�n de correo electr�nico con el link que necesitas clicar para cambiar tu contraseña.<br>

<br>

�NOTA!: Recuerda comprobar tambi�n la carpeta de 'Spam'</span>
                <div id="email-sent-container"></div>
            </div>
            <div class="change-password-buttons">
                <a href="#" id="change-password-change-link">Atr�s</a>
                <a href="#" id="change-password-success-button" class="new-button"><b>Cerrar</b><i></i></a>
            </div>

        </div>
    </div>
    <div id="change-password-form-container-bottom"></div>
</div>

<script type="text/javascript">
HabboView.add( function() {
     ChangePassword.init();


});
</script>
<div id="container">
	<div class="cbb process-template-box clearfix">
		<div id="content" class="wide">
					<div id="header" class="clearfix">

						<h1><a href="<?php echo PATH; ?>/"></a></h1>
<ul class="stats">
</ul>
					</div>
			<div id="process-content">
	        	<div class="cbb clearfix">
    <h2 class="title">�Hecho!</h2>

    <div class="box-content">
    <p>Tu contraseña ha sido cambiada con �xito.</p>
    <p><a href="<?php echo PATH; ?>">Volver a la Home &raquo;</a></p>
    </div>
</div>
<div id="footer">
	<p class="footer-links"><a href="https://help.habbo.es" target="_new">Cont�ctanos</a> | <a href="https://help.habbo.es/forums" target="_new">FAQs</a> | <a href="http://www.sulake.com" target="_new">Sulake</a> | <a href="https://help.habbo.es/entries/368102-terminos-y-condiciones-del-servicio" target="_new">T�rminos y Condiciones</a> | <a href="https://help.habbo.es/entries/368010-politica-de-privacidad" target="_new">Pol�tica de Privacidad</a>  | <a href="<?php echo PATH; ?>/groups/GuiaparaPadres" target="_new">Gu�a para Padres</a>  | <a href="<?php echo PATH; ?>/groups/_LaManeraHabbo" target="_new">La Manera Habbo</a>  | <a href="<?php echo PATH; ?>/groups/ConsejosdeSeguridad" target="_new">Seguridad</a>  | <a href="mailto:advertising.es@sulake.com" target="_new">Publicidad</a></p>

	<p class="copyright">� 2010 Sulake Corporation Oy. HABBO es una marca registrada de Sulake Corporation Oy en la Uni�n Europea, Estados Unidos, Jap�n, la Rep�blica Popular China y otras jurisdicciones. Todos los derechos reservados.</p>
</div>			</div>
        </div>
    </div>
</div>
<script type="text/javascript">
if (typeof HabboView != "undefined") {
	HabboView.run();
}
</script>


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-448325-19']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>    <!-- START Nielsen Online SiteCensus V6.0 -->

<!-- COPYRIGHT 2009 Nielsen Online -->
<script type="text/javascript" src="//secure-uk.imrworldwide.com/v60.js">
</script>
<script type="text/javascript">
 var theCid = "es-widantena3tv";
 if (top == self) {
   theCid = "es-antena3tv";
 }
 var pvar = { cid: theCid, content: "0", server: "secure-uk" };
 var trac = nol_t(pvar);
 trac.record().post();
</script>
<!-- END Nielsen Online SiteCensus V6.0 -->
    

    



</body>
</html>