<?php
$body_class = 'process-template black secure-page';
$no_id = true;

require '../../../KERNEL-XDRCMS/Init.php';
require HEADER . 'password.php';

if(isset($_POST['token']) || isset($_GET['Code'])):
	$_POST['token'] = (isset($_GET['Code'])) ? $_GET['Code'] : $_POST['token'];
	$Token = md5($_POST['token']);
	
	$q = $MySQLi->query('SELECT * FROM xdrcms_users_keys WHERE Code = \'' . $Token . '\' AND Type = \'Recuper\' LIMIT 1');

	if(!$q || $q->num_rows === 0):
		require HTML . 'Error_Password.html';
		goto a;
	endif;
	
	$r = $q->fetch_assoc();
	
	if((time() - $r['Time']) > 86400):
		$MySQLi->query('DELETE FROM xdrcms_users_keys WHERE Code = \'' . $Token . '\' LIMIT 1');

		require HTML . 'Error_PasswordExpired.html';
		goto a;
	endif;

	if(isset($_POST['password'], $_POST['retypedPassword'])):
		if(empty($_POST['password'])):
			$e = 'Please enter a password';
		elseif(strlen($_POST['password']) < 6):
			$e = 'Your password needs be at least 6 characters long';
		elseif(strlen($_POST['password']) > 32):
			$e = 'Your password is too long';
		elseif(!preg_match('`[0-9]`', $_POST['password'])):
			$e = 'Your password must also include numbers';
		elseif($_POST['password'] !== $_POST['retypedPassword']):
			$e = 'The passwords do not match';
		else:
			$MySQLi->query('UPDATE xdrcms_users_data SET password = \'' . METHOD::HASH($_POST['password']) . '\' WHERE mail = \'' . $r["UserEmail"] . '\'');
			$MySQLi->query('DELETE FROM xdrcms_users_keys WHERE Code = \'' . $Token . '\' LIMIT 1');
			echo '<META HTTP-EQUIV="REFRESH" CONTENT="0;URL=' . PATH . '/account/password/resetConfirmation">';
			exit();
		endif;
	endif;

	require HTML . 'Community_ResetPassword.html';
else:
	require HTML . 'Error_Password.html';
endif;

a:
require FOOTER . 'community.php';
?>