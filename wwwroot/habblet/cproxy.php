<?php
if(!isset($_GET['habbletKey'], $_POST['fromHabblet']) || !in_array($_GET['habbletKey'], ['roomenterad', 'news', 'minimail'])):
	header('HTTP/1.1 403 Forbidden');
	return;
endif;
require '../../KERNEL-XDRCMS/Init.php';

/*
	if($_GET["habbletKey"] == "avatars"):
		$cProxy = new Site();
		$cProxy->AppendHTML("HTML-Page_avatars.html", CPROXY);
		$cProxy->Show(SIMPLE);
*/
if($_GET['habbletKey'] === 'roomenterad'):
		//header ('X-JSON: {"disabled":"true"}');
		$aSettings = CACHE::GetAIOConfig('ADS');
		
		if(empty($aSettings['roomenterad']) || rand(0, 5) !== 5)
			exit;
		$_SESSION['RommEnterAd'] = (isset($_SESSION['RommEnterAd'])) ? ++$_SESSION['RommEnterAd'] : 1;
		
		if($_SESSION['RommEnterAd'] < 4):
			require HTML . 'cProxy_RommEnterAd.html';
		endif;
elseif($_GET['habbletKey'] === 'minimail'):
		require HTML . 'cProxy_Minimail.html';
elseif($_GET['habbletKey'] === 'news'):
		require HTML . 'cProxy_Articles.html';
endif;
?>