<?php
if(!isset($_GET['ID'], $_GET['SubID']))	exit;
require '../../KERNEL-XDRCMS/Init.php';

if($_GET['SubID'] == 1):
	$product = 'The Starter Room';
	$price = '$1.00';
elseif($_GET['SubID'] == 2):
	$img = webgallery . '/images/v3/creditflow/credits_icon.png';
	$product = '<span class="multiplier">x</span> 60 Credits';
	$price = '$9.95';
elseif($_GET['SubID'] == 3):
	$img = webgallery . '/images/v3/creditflow/credits_icon.png';
	$product = '<span class="multiplier">x</span> 180 Credits';
	$price = '$29.95';
else:
	exit;
endif;
?>
<div class="modal" id="confirmation-modal">
    <h2>Payment details</h2>
    <div id="confirmation-container">
        <div id="confirm-details-wrapper">
            <div id="confirm-details-top">
                <div class="confirm-product">
                    <p>Product</p>
                </div>
                <div class="confirm-price">
                    <p>Price</p>
                </div>
                <div class="confirm-recipient">
                    <p>Habbo Name</p>
                </div>
            </div>
            <div id="confirm-details">
                <div class="confirm-product">
                    <div class="confirm-product">
                        <p>
							<?php if(isset($img)): ?><img src="<?php echo $img; ?>" alt=""><?php endif; ?>
							<?php echo $product; ?>
						</p>
                    </div>
                </div>
                <div class="confirm-price">
                    <p>
                            <?php echo $price; ?>
                    </p>
                </div>
                <div class="confirm-recipient">
                    <p><?php echo USER::$Data['Name']; ?></p>
                </div>
            </div>
        </div>
        <div id="purchase-options-lower">
            <p class="confirm-email">
                <span class="username"><?php echo USER::$Data['Name']; ?></span>, confirm your email address
            </p>
            <p class="confirm-email-input"><input class="flat-input blue" type="text"
                                                  name="confirmEmailAddress" id="confirm-field" value="<?php echo USER::$Data['Email']; ?>" placeholder="Forgot something?"></p>
            <hr class="popup-hr">


            <h1>Pay with</h1>
            <div id="purchase-buttons">
                        <input class="confirm-button logo proceed-with-payment" data-country-id="1" data-method-id="1" data-price-point-id="<?php echo $_GET['SubID']; ?>" type="button" style="background: #00753F url(//habboo-a.akamaihd.net/c_images/cbs2_partner_logos/partner_logo_credit_card_005.png?h=6187207968649e80689770c84ae75f92) no-repeat center" value="&nbsp;" />
                        <input class="confirm-button logo proceed-with-payment" data-country-id="1" data-method-id="2" data-price-point-id="<?php echo $_GET['SubID']; ?>" type="button" style="background: #00753F url(//habboo-a.akamaihd.net/c_images/cbs2_partner_logos/partner_logo_paypal_001.png?h=3fc422a5606f68c0cebfafeb216b22cb) no-repeat center" value="&nbsp;" />
            </div>
            <div class="disclaimer">
            <h3>Important Message</h3>
                <p>Always ask the bill-payer's permission first. If you don't and the payment is later canceled or declined, you'll be banned.<br /> All legitimate ways to buy Credits are shown here. Buying them elsewhere may get you ripped off and banned. <br/>The digital content is delivered immediately and by purchase it is accepted that there is no right of withdrawal.</p>
            </div>
            <p>
                <span class="credits-terms-desc">By proceeding you accept our</span> <a href="https://help.habbo.com/entries/23096348-Terms-of-Service-and-Privacy-Policy" target="_blank" class="credits-terms-link">Terms and Conditions</a>
            </p>
        </div>
        <img src="<?php echo webgallery; ?>/images/v3/creditflow/confirm_car.png" id="confirm-car" alt="">
        <p id="close-popup"><a href="#" rel="modal:close">&times;</a></p>
    </div>
</div>