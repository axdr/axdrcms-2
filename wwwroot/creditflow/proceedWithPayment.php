<?php
if(!isset($_GET['ID']))	exit;
require '../../KERNEL-XDRCMS/Init.php';
$noNAV = true;
require HEADER . 'community.v3.php';

if($_GET['ID'] == 1):
	$paymentID = '1387140695346';
elseif($_GET['ID'] == 2):
	$paymentID = '1387140695346';
elseif($_GET['ID'] == 3):
	$paymentID = '1387140695346';
else:
	exit;
endif;
?>
<div id="content">
    <div id="page-content">
        <section id="left-content">
    <img src="<?php echo webgallery; ?>/images/v3/creditflow/confirm_car.png" alt="">
</section>

<section id="right-content">

    <h1 id="success">Redirecting to selected payment partner</h1>

    <hr class="divider"/>

    <p>
        You are automatically being redirected to the selected payment partner.
    </p>

    <p>
        <img src="<?php echo webgallery; ?>/images/v3/creditflow/loadbar.gif" alt="..."/>
    </p>

	<p>
		Owner, visit the file /creditflow/proceedWithPayment.php for more information.
	</p>
<?php
################## DEVELOPERS#################
# EDIT THIS FOR YOUR PAYMENT METHOD
?>
    <form id="startPaymentForm" method="post" action="https://billing.sulake.com/payment-integrations/online/hhus_wp_paypal/user/executePayment">
        <input type="hidden" name="f2ee_txid" value="288753985">
        <input type="hidden" name="id" value="<?php echo $paymentID; ?>">
        <input type="hidden" name="token" value="f70d0bf7ddc2b4fa5f4d172e310fd6ba1e10eab520288b80eeb708efc05dcc31">
    </form>

    <script type="text/javascript">
        $(document).ready(function() {
            setTimeout(function() {
                $("#startPaymentForm").submit();
            }, 100);
        });
    </script>

</section>
    </div>
</div>


<script src="<?php echo webgallery ?>/static/js/v3_default_bottom.js" type="text/javascript"></script>
<!--[if IE]><script src="<?php echo webgallery ?>/static/js/v3_ie_fixes.js" type="text/javascript"></script>
<![endif]-->
<?php
require FOOTER . 'community.v3.php';
?>